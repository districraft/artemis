//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.entity.mcserver;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.enums.mcserver.BanType;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Ban {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String targetUUID;
    @Column(nullable = false)
    private String targetUsername;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String reason = "The ban hammer has spoken";
    @Column(nullable = false)
    private BanType type = BanType.PERM;
    @Column(nullable = true)
    private Timestamp expiresAt;
    @Column(nullable = false)
    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
    @Column(nullable = true)
    private Timestamp updatedAt;
    @Column(nullable = false)
    private String sourceUsername;
    @Column(nullable = false)
    private String sourceUUID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTargetUUID() {
        return targetUUID;
    }

    public void setTargetUUID(String targetUUID) {
        this.targetUUID = targetUUID;
    }

    public String getTargetUsername() {
        return targetUsername;
    }

    public void setTargetUsername(String targetUsername) {
        this.targetUsername = targetUsername;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BanType getType() {
        return type;
    }

    public void setType(BanType type) {
        this.type = type;
    }

    public Timestamp getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Timestamp expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSourceUsername() {
        return sourceUsername;
    }

    public void setSourceUsername(String sourceUsername) {
        this.sourceUsername = sourceUsername;
    }

    public String getSourceUUID() {
        return sourceUUID;
    }

    public void setSourceUUID(String sourceUUID) {
        this.sourceUUID = sourceUUID;
    }
}
