//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis;

import com.github.slugify.Slugify;
import org.districraft.artemis.entity.ArtemisConfig;
import org.districraft.artemis.entity.auth.MinecraftProfile;
import org.districraft.artemis.entity.auth.Profile;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.repository.ArtemisConfigRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@SpringBootApplication
public class ArtemisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtemisApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadData(ArtemisConfigRepository confRepo,
									  UserRepository userRepo,
									  PasswordEncoder encoder) {

        Optional<ArtemisConfig> isSeeded = confRepo.findByConfKey("isSeeded");
		ArtemisConfig conf;

		conf = isSeeded.orElseGet(() -> new ArtemisConfig("isSeeded", "false"));

		if(conf.getConfVal().equalsIgnoreCase("true"))
			return (args) -> {};

		return (args) -> {
			conf.setConfVal("true");

			MinecraftProfile mcProf = new MinecraftProfile();
			mcProf.setName("admin");
			mcProf.setUuid("");
			mcProf.setDemo(false);
			mcProf.setLegacy(false);

			Profile prof = new Profile();
			prof.setMinecraftProfile(mcProf);
			prof.setRealName("Admin Admin");
			prof.setAvatar("https://placehold.it/500");

			User user = new User();
			user.setUsername("admin");
			user.setRole("ADMIN");
			user.setProfile(prof);
			user.setEmail("email@example.com");
			user.setPassword(encoder.encode("P@ssw0rd!"));
			user.setActivated(true);

			userRepo.save(user);
			confRepo.save(conf);
		};
	}

//	@Bean
//	public Bifrost getBifrost() {
//		return new Bifrost();
//	}

	@Bean
	public Slugify getSlugify() {
		return new Slugify();
	}
}
