.PHONY: package-prod prepare-docker run-docker clean-docker docker clean-docs build-docs deploy-docs docs

MAVEN=mvn
SKIP_TESTS=-Dmaven.test.skip=true

ARTIFACT_NAME=artemis-*.jar

package-prod:
	 ${MAVEN} ${SKIP_TESTS} clean package spring-boot:repackage

prepare-flyway:
	rm -rf docker/artemis/flyway \
	&& cp -r src/main/resources/flyway docker/artemis/flyway

prepare-docker:
	rm -f docker/artemis/artemis.jar \
	&& cp target/${ARTIFACT_NAME} docker/artemis/artemis.jar

run-docker:
	cd docker && docker-compose up --build artemis nginx

clean-docker:
	cd docker && docker-compose kill && docker-compose rm -f

clean-docs:
	rm -rf ./public ./docs/output

build-docs:
	cd ./docs && jbake -b

deploy-docs:
	cp -r ./docs/output ./public

docs: clean-docs build-docs deploy-docs

docker: clean-docker package-prod prepare-flyway prepare-docker run-docker
