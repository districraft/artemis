package org.districraft.artemis.repository.security;

import org.districraft.artemis.entity.security.AuditLogEntry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditLogEntryRepository extends JpaRepository<AuditLogEntry, Long> {
}
