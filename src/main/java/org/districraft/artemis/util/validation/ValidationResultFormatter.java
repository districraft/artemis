//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util.validation;

import com.google.gson.Gson;

public class ValidationResultFormatter {
    public static String formatAsPlaintext(String[] errorMessages, String separator) {
        StringBuilder result = new StringBuilder();

        for(String message : errorMessages) {
            result.append(message);
            result.append(separator);
        }

        result.setLength(result.length() - 1);

        return result.toString();
    }

    public static String formatAsHTMLList(String[] errorMessages) {
        StringBuilder result = new StringBuilder();

        result.append("<ul>\n");

        for(String message : errorMessages) {
            result.append("<li>");
            result.append(message);
            result.append("</li>\n");
        }

        result.append("</ul>");

        return result.toString();
    }

    public static String formatAsJSON(String[] errorMessages) {
        Gson gson = new Gson();
        return gson.toJson(errorMessages);
    }
}
