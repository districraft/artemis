//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.repository.permission;

import org.districraft.artemis.entity.permission.PermissionAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PermissionAssignmentRepository extends JpaRepository<PermissionAssignment, Long> {
    @Query(
            value = "SELECT EXISTS(SELECT pa.id AS is_present FROM permission_assignment AS pa LEFT JOIN permission_node pn on pa.node_id = pn.id WHERE pn.node = ?1 AND pa.user_id = ?2)",
            nativeQuery = true)
    Long existsByUserAndNode(String node, Long user);

    @Query(
            value = "SELECT EXISTS(SELECT pa.id AS is_present FROM permission_assignment AS pa LEFT JOIN permission_node pn on pa.node_id = pn.id WHERE pn.node = ?1 AND pa.group_id = ?2)",
            nativeQuery = true)
    Long existsByGroupAndNode(String node, Long group);
}
