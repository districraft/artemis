//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.tags;

import org.districraft.artemis.entity.issue.IssueReport;
import org.districraft.artemis.enums.IssueResolutionStatus;
import org.districraft.artemis.repository.issue.IssueReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

import java.text.MessageFormat;
import java.util.Optional;

public class IssueOperationsButtonsTagProcessor extends AbstractAttributeTagProcessor {
    private static final String ATTR_NAME = "issueButtons";
    private static final int PRECEDENCE = 10000;

    private final IssueReportRepository issueReportRepository;

    private Logger log = LoggerFactory.getLogger(IssueOperationsButtonsTagProcessor.class);

    public IssueOperationsButtonsTagProcessor(final String dialectPrefix, final IssueReportRepository issueReportRepository) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTR_NAME,
                true,
                PRECEDENCE,
                true
        );

        this.issueReportRepository = issueReportRepository;
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext, IProcessableElementTag iProcessableElementTag, AttributeName attributeName, String s, IElementTagStructureHandler iElementTagStructureHandler) {

        final IEngineConfiguration configuration = iTemplateContext.getConfiguration();
        final IStandardExpressionParser parser = StandardExpressions.getExpressionParser(configuration);
        final IStandardExpression expression = parser.parseExpression(iTemplateContext, s);
        final Long id = (Long) expression.execute(iTemplateContext);

        Optional<IssueReport> oReport = issueReportRepository.findById(id);

        if(!oReport.isPresent()) {
            iElementTagStructureHandler.setBody("Could not load operations buttons", false);
            return;
        }

        IssueReport report = oReport.get();
        StringBuilder buttons = new StringBuilder();

        buttons.append("<div class=\"btn-list\">");

        if(report.getResolutionStatus() != IssueResolutionStatus.OPEN &&
                report.getResolutionStatus() != IssueResolutionStatus.IN_PROGRESS) {

            buttons.append(MessageFormat.format("<a href=\"/api/v1/issues/reopen/{0}\" class=\"btn btn-azure\">Reopen</a>", report.getId()));
        }

        if(report.getResolutionStatus() != IssueResolutionStatus.IN_PROGRESS &&
            report.getResolutionStatus() != IssueResolutionStatus.CLOSED &&
            report.getResolutionStatus() != IssueResolutionStatus.WONT_FIX &&
            report.getResolutionStatus() != IssueResolutionStatus.REJECTED) {

            buttons.append(MessageFormat.format("<a href=\"/api/v1/issues/start-work/{0}\" class=\"btn btn-azure\">Start Work</a>", report.getId()));
        }

        if(report.getResolutionStatus() == IssueResolutionStatus.IN_PROGRESS) {

            buttons.append(MessageFormat.format("<a href=\"/api/v1/issues/resolve/{0}\" class=\"btn btn-green\">Resolve</a>", report.getId()));
        }

        if(report.getResolutionStatus() != IssueResolutionStatus.WONT_FIX &&
            report.getResolutionStatus() != IssueResolutionStatus.CLOSED &&
            report.getResolutionStatus() != IssueResolutionStatus.REJECTED) {

            buttons.append(MessageFormat.format("<a href=\"/api/v1/issues/wont-fix/{0}\" class=\"btn btn-orange\">Won't Fix</a>", report.getId()));
        }

        if(report.getResolutionStatus() != IssueResolutionStatus.REJECTED &&
            report.getResolutionStatus() != IssueResolutionStatus.CLOSED &&
        report.getResolutionStatus() != IssueResolutionStatus.WONT_FIX) {

            buttons.append(MessageFormat.format("<a href=\"/api/v1/issues/reject/{0}\" class=\"btn btn-red\">Reject</a>", report.getId()));
        }

        buttons.append("</div>");

        iElementTagStructureHandler.setBody(buttons.toString(), false);
    }
}
