//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.enums;

public enum IssueCategory {
    GAME(0, "In-Game Issue"),
    BUG(1, "Bug"),
    IDEA(2, "Idea"),
    MISC(3, "Other");

    private int val;
    private String displayName;

    IssueCategory(int val, String displayName) {
        this.val = val;
        this.displayName = displayName;
    }

    public int getNumericalValue() {
        return this.val;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
