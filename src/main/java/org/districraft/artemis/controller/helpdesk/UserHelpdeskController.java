//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.helpdesk;

import org.districraft.artemis.entity.issue.IssueComment;
import org.districraft.artemis.entity.issue.IssueReport;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.enums.IssueCategory;
import org.districraft.artemis.enums.IssueEscalationLevel;
import org.districraft.artemis.enums.IssueResolutionStatus;
import org.districraft.artemis.enums.IssueSeverity;
import org.districraft.artemis.repository.issue.IssueCommentRepository;
import org.districraft.artemis.repository.issue.IssueReportRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.districraft.artemis.util.InsufficientPermissionException;
import org.districraft.artemis.util.PermissionHelper;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller()
public class UserHelpdeskController {
    private IssueReportRepository issueReportRepository;
    private IssueCommentRepository issueCommentRepository;
    private UserRepository userRepository;
    private PermissionHelper permissionHelper;

    public UserHelpdeskController(IssueReportRepository issueReportRepository,
                                  IssueCommentRepository issueCommentRepository,
                                  UserRepository userRepository,
                                  PermissionHelper permissionHelper) {
        this.issueReportRepository = issueReportRepository;
        this.issueCommentRepository = issueCommentRepository;
        this.userRepository = userRepository;
        this.permissionHelper = permissionHelper;
    }

    // TODO: DCXARTEMIS-44 -- Issue Tracker is disabled until other stuff is done

    @GetMapping("/issues/")
    public String getIndex() throws ResourceNotFoundException {
        throw new ResourceNotFoundException();
    }

//    @GetMapping("/issues/")
//    public String getIndex(Model model) throws InsufficientPermissionException {
//
//        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User user = principal.getUser();
//
//        List<IssueReport> reportList;
//
//        if (permissionHelper.userFromContextHasPermissionByGroup("issues.view.any")) {
//            reportList = issueReportRepository.findAll();
//        } else if (permissionHelper.userFromContextHasPermissionByGroup("issues.view.group")) {
//            if (user.getRole().equals("USER")) throw new InsufficientPermissionException(33554);
//
//            reportList = issueReportRepository.findAllByEscalationLevel(
//                    IssueEscalationLevel.valueOf(user.getRole()));
//        } else if (permissionHelper.userFromContextHasPermissionByGroup("issues.view.own")) {
//            reportList = issueReportRepository.findAllByAuthor(user);
//        } else {
//            throw new InsufficientPermissionException(33555);
//        }
//
//        model.addAttribute("issues", reportList);
//        model.addAttribute("activePage", "/issues");
//        return "helpdesk/index.html";
//    }
//
//    @GetMapping("/issues/new")
//    public String getNewIssue(
//            @RequestParam(name = "issueType", required = false, defaultValue = "none") String issueType,
//            @RequestParam(name = "issueTitle", required = false, defaultValue = "") String issueTitle,
//            Model model) throws InsufficientPermissionException {
//
//        if (!permissionHelper.userFromContextHasPermissionByGroup("issues.new"))
//            throw new InsufficientPermissionException(33446);
//
//        if (issueType != null && !issueType.equals("none")) {
//            try {
//                model.addAttribute("issueType", IssueCategory.valueOf(issueType).getNumericalValue());
//            } catch (IllegalArgumentException e) {
//                model.addAttribute("issueType", "Bug");
//            }
//
//        } else {
//            model.addAttribute("issueType", "Bug");
//        }
//
//        model.addAttribute("issueTitle", issueTitle);
//
//        return "helpdesk/new.html";
//    }
//
//    @PostMapping("/issues/create")
//    public String postCreateIssue(
//            @RequestParam(name = "issueName") String issueName,
//            @RequestParam(name = "issueDescription") String issueDescription,
//            @RequestParam(name = "issueSeverity") String issueSeverity,
//            @RequestParam(name = "issueType") String issueType,
//            RedirectAttributes redirectAttributes
//    ) throws InsufficientPermissionException {
//
//        if (!permissionHelper.userFromContextHasPermissionByGroup("issues.new"))
//            throw new InsufficientPermissionException(33446);
//
//        List<String> errors = new ArrayList<>();
//
//        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User user = principal.getUser();
//        Optional<User> oUser = userRepository.findById(user.getId());
//
//        if (!oUser.isPresent()) throw new InsufficientPermissionException(55441);
//
//        user = oUser.get();
//
//        IssueReport report = new IssueReport();
//
//        if (issueName == null || issueName.isEmpty()) errors.add("The issue title must not be empty.");
//        if (issueDescription == null || issueDescription.isEmpty()) errors.add("The description must not be empty.");
//        if (issueSeverity == null || issueSeverity.isEmpty()) errors.add("The issue severity must be specified.");
//        if (issueType == null || issueType.isEmpty()) errors.add("The issue type must be specified.");
//
//        if (!errors.isEmpty()) {
//            StringBuilder err = new StringBuilder();
//            errors.forEach((x) -> {
//                err.append("<ul>\n");
//                err.append("<li>").append(x).append("</li>\n");
//                err.append("</ul>\n");
//            });
//            redirectAttributes.addFlashAttribute("error", err.toString());
//            return "redirect:/issues/new";
//        }
//
//        report.setTitle(issueName);
//        report.setDescription(issueDescription);
//        report.setAuthor(user);
//
//        try {
//            report.setCategory(IssueCategory.valueOf(issueType));
//        } catch (Exception e) {
//            report.setCategory(IssueCategory.BUG);
//        }
//
//        try {
//            report.setSeverity(IssueSeverity.valueOf(issueSeverity));
//        } catch (Exception e) {
//            report.setSeverity(IssueSeverity.LOW);
//        }
//
//        report.setResolutionStatus(IssueResolutionStatus.OPEN);
//        report.setEscalationLevel(IssueEscalationLevel.MOD);
//        report.setCreatedAt(new Timestamp(System.currentTimeMillis()));
//
//        report = issueReportRepository.save(report);
//
//        redirectAttributes.addFlashAttribute("success", "Issue was created.");
//        return "redirect:/issues/" + report.getId();
//    }
//
//    @GetMapping("/issues/{id}")
//    public String getSingleIssue(@PathVariable int id, Model model) throws ResourceNotFoundException, InsufficientPermissionException {
//        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User user = principal.getUser();
//
//        Optional<IssueReport> oReport = issueReportRepository.findById((long) id);
//
//        if (!oReport.isPresent()) throw new ResourceNotFoundException();
//
//        IssueReport report = oReport.get();
//
//        // If user doesn't have any view permission
//        if (!permissionHelper.userFromContextHasPermissionByGroup("issues.view.own") &&
//                        !permissionHelper.userFromContextHasPermissionByGroup("issues.view.group") &&
//                        !permissionHelper.userFromContextHasPermissionByGroup("issues.view.any")) {
//            throw new InsufficientPermissionException(47756);
//        }
//
//
//        if (permissionHelper.userFromContextHasPermissionByGroup("issues.view.group")) {
//            if (user.getRole().equals("MOD")) {
//                if (report.getEscalationLevel() != IssueEscalationLevel.MOD) {
//                    throw new InsufficientPermissionException(47757);
//                }
//            } else if (user.getRole().equals("GM")) {
//                if (!Arrays.asList("MOD", "GM").contains(report.getEscalationLevel().getName())) {
//                    throw new InsufficientPermissionException(47758);
//                }
//            }
//        } else if (permissionHelper.userFromContextHasPermissionByGroup("issues.view.own")) {
//            if (!report.getAuthor().getId().equals(user.getId())) {
//                throw new InsufficientPermissionException(47759);
//            }
//        }
//
//        List<IssueComment> comments = issueCommentRepository.findByReportOrderByCreatedAtAsc(report);
//
//        model.addAttribute("issue", report);
//        model.addAttribute("comments", comments);
//
//        return "helpdesk/single.html";
//    }
}
