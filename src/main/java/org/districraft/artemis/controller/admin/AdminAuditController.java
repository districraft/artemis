package org.districraft.artemis.controller.admin;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.entity.security.AuditLogEntry;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.repository.security.AuditLogEntryRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.districraft.artemis.service.AuditService;
import org.districraft.artemis.util.InsufficientPermissionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class AdminAuditController {
    private AuditLogEntryRepository auditLogEntryRepository;
    private AuditService auditService;
    private UserRepository userRepository;

    @Value("${artemis.audit.preserveForDays}")
    private Long protectForDays;

    public AdminAuditController(AuditLogEntryRepository auditLogEntryRepository, AuditService auditService, UserRepository userRepository) {
        this.auditLogEntryRepository = auditLogEntryRepository;
        this.auditService = auditService;
        this.userRepository = userRepository;
    }

    @GetMapping("/admin/audit")
    public String getIndex(Model model) {
        model.addAttribute("log", auditLogEntryRepository.findAll(Sort.by(Sort.Direction.DESC, "tStamp")));

        Timestamp now = new Timestamp(System.currentTimeMillis());
        ZonedDateTime zonedDateTime = now.toInstant().atZone(ZoneId.of("UTC"));
        Timestamp protectionDate = Timestamp.from(zonedDateTime.minus(protectForDays, ChronoUnit.DAYS).toInstant());
        model.addAttribute("deleteOlderThan", protectionDate);
        model.addAttribute("activePage", "admin/audit");

        return "admin/audit/index.html";
    }

    @GetMapping("/admin/audit/plaintext")
    public void getAsText(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "text/plain; charset=utf-8");
        response.setStatus(200);

        List<AuditLogEntry> auditLogEntries = auditLogEntryRepository.findAll();

        StringBuilder sb = new StringBuilder();

        sb.append("<<< Artemis Audit Log >>>\n");
        sb.append("=========================\n");

        auditLogEntries.forEach((x) -> {
            String sourceUser = (x.getSourceUser() == null)? "null" : x.getSourceUser().getUsername();
            sb.append(MessageFormat.format("AuditLogEntry[TransactionID: {0}; ActionType: {1}; ActionDetail: {2}; ClassName: {3}; SecurityLevel: {4}; Timestamp: {5}; User: {6}]\n",
                    x.getId(), x.getActionType().name(), x.getActionDetail(), x.getClassName(), x.getSecurityLevel().name(), x.gettStamp().toString(), sourceUser));
        });

        PrintWriter out = response.getWriter();
        out.print(sb.toString());
    }

    @GetMapping("/admin/audit/json")
    public void getAdJson(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json; charset=utf-8");
        response.setStatus(200);

        List<AuditLogEntry> auditLogEntries = auditLogEntryRepository.findAll();

        ExclusionStrategy strategy = new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                return fieldAttributes.getName().equalsIgnoreCase("sourceUser");
            }

            @Override
            public boolean shouldSkipClass(Class<?> aClass) {
                return aClass.isAssignableFrom(User.class);
            }
        };

        PrintWriter out = response.getWriter();
        out.print(new GsonBuilder().addSerializationExclusionStrategy(strategy).create().toJson(auditLogEntries));
    }

    @GetMapping("/admin/audit/autoclean")
    public String getCleanAuditLog(RedirectAttributes redirectAttributes) throws InsufficientPermissionException {
        List<AuditLogEntry> entryList = auditLogEntryRepository.findAll();

        Timestamp now = new Timestamp(System.currentTimeMillis());
        ZonedDateTime zonedDateTime = now.toInstant().atZone(ZoneId.of("UTC"));
        Timestamp protectionDate = Timestamp.from(zonedDateTime.minus(protectForDays, ChronoUnit.DAYS).toInstant());

        List<AuditLogEntry> toDelete = entryList.stream().filter((x) -> x.gettStamp().before(protectionDate)).collect(Collectors.toList());

        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = principal.getUser();
        Optional<User> oUser = userRepository.findById(user.getId());

        if (!oUser.isPresent()) throw new InsufficientPermissionException(222);

        auditLogEntryRepository.deleteAll(toDelete);

        auditService.log(AuditActionType.SYSTEM,
                AuditActionSecurityLevel.SYSTEM,
                MessageFormat.format("The audit log has been purged by {0} at {1}, deleting items older than {2}",
                        oUser.get().getUsername(),
                        now.toString(),
                        protectionDate.toString()),
                AdminAuditController.class,
                null,
                oUser.get());

        redirectAttributes.addFlashAttribute("success", "The log has been cleaned");

        return "redirect:/admin/audit";
    }
}
