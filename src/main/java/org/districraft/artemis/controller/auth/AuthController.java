//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.auth;

import org.districraft.artemis.entity.auth.PasswordResetLink;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.repository.auth.PasswordResetLinkRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.service.GenericEmailSender;
import org.districraft.artemis.service.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AuthController {
    private UserRepository userRepository;
    private RandomStringGenerator randomStringGenerator;
    private PasswordResetLinkRepository passwordResetLinkRepository;
    private GenericEmailSender genericEmailSender;
    private PasswordEncoder encoder;

    @Value("${artemis.app.baseUrl}")
    private String baseUrl;

    private static final String PASSWORD_REGEX = "^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[.!@#$%^&*\\(\\)\\[\\]\\{\\}\\\\\\/\\,]).{6,20})$";


    public AuthController(UserRepository userRepository,
                          RandomStringGenerator randomStringGenerator,
                          PasswordResetLinkRepository passwordResetLinkRepository,
                          GenericEmailSender genericEmailSender,
                          PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.randomStringGenerator = randomStringGenerator;
        this.passwordResetLinkRepository = passwordResetLinkRepository;
        this.genericEmailSender = genericEmailSender;
        this.encoder = encoder;
    }

    @GetMapping("/login")
    public String login(Model model, @RequestParam(required = false) boolean error) {
        model.addAttribute("error", error);
        return "auth/login";
    }

    @GetMapping("/password-reset")
    public String getPasswordReset() {
        return "auth/reset.html";
    }

    @PostMapping("/perform_password_reset")
    public String postPasswordReset(@RequestParam(name = "email") String email, RedirectAttributes redirectAttributes) {
        if (email == null || email.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Invalid e-email address");

            return "redirect:/password-reset";
        }

        Optional<User> oUser = userRepository.findByEmail(email);

        if (oUser.isPresent()) {
            User user = oUser.get();

            PasswordResetLink link = new PasswordResetLink();

            String token = randomStringGenerator.nextString();

            // If the token already exists, try a new one. Realistically this will probably never happen...
            while(passwordResetLinkRepository.findByRecoveryCode(token).isPresent()) {
                token = randomStringGenerator.nextString();
            }

            link.setRecoveryCode(token);
            link.setUser(user);
            link.setEmail(user.getEmail());

            passwordResetLinkRepository.save(link);

            StringBuilder messageBody = new StringBuilder();
            messageBody.append("<h2>Here's your Artemis password recovery link.</h2>\n");
            messageBody.append("<p>Click the link below to reset your password.</p>\n");
            messageBody.append("<p><small>The link is only valid for 24 hours!</small></p>\n");
            messageBody.append("<hr>\n");
            messageBody.append(MessageFormat.format("<p><a href=\"{0}/password-recovery?recoveryKey={1}\">Reset your password</a></p>\n", baseUrl, link.getRecoveryCode()));

            try {
                genericEmailSender.sendGenericEmail("Artemis Password Reset",
                        messageBody.toString(), true, email);
            } catch (MessagingException ignored) {}
        }

        redirectAttributes.addFlashAttribute("success", "We've sent the password recovery instructions to your e-mail.");
        return "redirect:/password-reset";
    }

    @GetMapping("/password-recovery")
    public String getPasswordRecovery(@RequestParam(name = "recoveryKey", required = true) String recoveryKey, Model model) {
        if(recoveryKey == null || recoveryKey.isEmpty()) {
            return "redirect:/";
        }

        Optional<PasswordResetLink> oLink = passwordResetLinkRepository.findByRecoveryCode(recoveryKey);

        if(!oLink.isPresent()) {
            return "redirect:/";
        }

        PasswordResetLink link = oLink.get();

        model.addAttribute("recoveryCode", recoveryKey);

        return "auth/recovery.html";
    }

    @PostMapping("/perform_password_recovery")
    public String postPerformPasswordRecovery(@RequestParam(name = "recoveryKey") String recoveryKey,
                                              @RequestParam(name = "password") String password,
                                              @RequestParam(name = "password_again") String passwordCheck,
                                              RedirectAttributes redirectAttributes,
                                              HttpServletRequest request) {

        List<String> errors = new ArrayList<>();

        if(recoveryKey == null || recoveryKey.isEmpty()) errors.add("Invalid recovery key.");
        if(password == null || password.isEmpty()) errors.add("You need to specify a new password");
        if(passwordCheck == null || passwordCheck.isEmpty()) errors.add("You need to specify a new password");
        if(!password.equals(passwordCheck)) errors.add("The passwords must match.");
        if (password.isEmpty() || !password.matches(PASSWORD_REGEX)) errors.add("The specified password is to weak.");

        Optional<PasswordResetLink> oLink = passwordResetLinkRepository.findByRecoveryCode(recoveryKey);

        if(!oLink.isPresent()) {
            errors.add("The recovery key is invalid.");
        }

        if (!errors.isEmpty()) {
            StringBuilder err = new StringBuilder();
            errors.forEach((x) -> {
                err.append("<ul>\n");
                err.append("<li>").append(x).append("</li>\n");
                err.append("</ul>\n");
            });
            redirectAttributes.addFlashAttribute("error", err.toString());
            return "redirect:" + request.getHeader("Referer");
        }

        PasswordResetLink link = oLink.get();

        User user = link.getUser();

        user.setPassword(encoder.encode(password));

        userRepository.save(user);
        passwordResetLinkRepository.delete(link);

        redirectAttributes.addFlashAttribute("success", "Your password has been reset. You may now log in with your new password");

        return "redirect:/login";
    }
}
