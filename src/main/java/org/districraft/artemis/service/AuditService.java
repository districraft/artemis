package org.districraft.artemis.service;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.entity.security.AuditLogEntry;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.districraft.artemis.repository.security.AuditLogEntryRepository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class AuditService {
    private AuditLogEntryRepository repo;

    public AuditService(AuditLogEntryRepository auditLogEntryRepository) {
        this.repo = auditLogEntryRepository;
    }

    public void log(AuditActionType actionType, AuditActionSecurityLevel auditActionSecurityLevel, String actionDetail, Class<?> className) {
        this.log(actionType, auditActionSecurityLevel, actionDetail, className, null, null);
    }

    public void log(AuditActionType actionType,
                    AuditActionSecurityLevel auditActionSecurityLevel,
                    String actionDetail,
                    Class<?> className,
                    Timestamp timestamp,
                    User user) {

        AuditLogEntry entry = new AuditLogEntry();
        entry.setActionType(actionType);
        entry.setSecurityLevel(auditActionSecurityLevel);
        entry.setActionDetail(actionDetail);
        entry.setClassName(className.getName());

        if(timestamp != null) {
            entry.settStamp(timestamp);
        }

        if(user != null) {
            entry.setSourceUser(user);
        }

        repo.save(entry);
    }
}
