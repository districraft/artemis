//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.enums.statuspage;

public enum ServiceStatus {
    DOWN("Down", "danger"),
    INVESTIGATING("Investigating", "warning"),
    IDENTIFIED("Identified", "warning"),
    WATCHING("Watching", "info"),
    RESOLVED("Resolved", "success"),
    UNKNOWN("Unknown", "default");

    private String niceName;
    private String color;

    ServiceStatus(String niceName, String color) {
        this.niceName = niceName;
        this.color = color;
    }

    public String getNiceName() {
        return niceName;
    }

    public String getColor() {
        return color;
    }
}
