package org.districraft.artemis.enums;

public enum Authority {
    ANON("ANON"),
    USER("USER"),
    MOD("MOD"),
    GM("GM"),
    ADMIN("ADMIN");

    private String name;

    Authority(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
