#     Artemis
#     Copyright (C) 2020, Jakub Sycha
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

delete from permission_assignment where true;
delete from permission_node where true;
delete from permission_group where true;

INSERT INTO permission_group (id, name) VALUES
(1, 'ADMIN'),
(2, 'GM'),
(3, 'MOD'),
(4, 'USER');

INSERT INTO permission_node (id, node) VALUES
(1, 'issues.new'),
(2, 'issues.view.own'),
(3, 'issues.view.group'),
(4, 'issues.view.any'),
(5, 'issues.edit.own'),
(6, 'issues.edit.group'),
(7, 'issues.edit.any'),
(8, 'issues.delete.any'),
(9, 'issues.escalate.gm'),
(10, 'issues.escalate.admin'),
(11, 'issues.deescalate.gm'),
(12, 'issues.deescalate.mod'),
(13, 'issues.comment.own'),
(14, 'issues.comment.group'),
(15, 'issues.comment.any'),
(16, 'issues.deletecomment.own'),
(17, 'issues.deletecomment.any'),
(18, 'issues.editcomment.own'),
(19, 'issues.editcomment.any'),
(20, 'issues.changestatus.own'),
(21, 'issues.changestatus.any'),
(22, 'issues.assign.self'),
(23, 'issues.assign.others_below'),
(24, 'issues.assign.others_any');

-- User permissions
INSERT INTO permission_assignment (group_id, node_id) VALUES
(4, 1),
(4, 2),
(4, 5),
(4, 13),
(4, 16),
(4, 18);

-- Mod Permissions
INSERT INTO permission_assignment (group_id, node_id) VALUES
(3, 1),
(3, 3),
(3, 6),
(3, 9),
(3, 14),
(3, 16),
(3, 18),
(3, 20),
(3, 22),
(3, 23);

-- GM Permissions
INSERT INTO permission_assignment (group_id, node_id) VALUES
(2, 1),
(2, 3),
(2, 6),
(2, 10),
(2, 12),
(2, 14),
(2, 16),
(2, 18),
(2, 20),
(2, 22),
(2, 23);

-- Admin Permissions
INSERT INTO permission_assignment (group_id, node_id) VALUES
(1, 1),
(1, 4),
(1, 7),
(1, 8),
(1, 11),
(1, 12),
(1, 15),
(1, 17),
(1, 19),
(1, 21),
(1, 22),
(1, 24);


INSERT INTO permission_assignment (group_id, user_id)
SELECT id, 1
FROM users
WHERE username = 'admin';

UPDATE users
SET permission_group = 1
WHERE username = 'admin';


