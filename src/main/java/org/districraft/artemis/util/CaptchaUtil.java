//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class CaptchaUtil {

    @Value("${artemis.captcha.privateKey}")
    private String privateKey;
    @Value("${artemis.captcha.verifyUrl}")
    private String verifyUrl;

    public CaptchaUtil() {}

    public Optional<CaptchaResult> verify(String token) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("privatekey", privateKey);
        urlParams.put("token", token);

        String finalUrl = StringSubstitutor.replace(verifyUrl, urlParams, "{", "}");

        Request request = new Request.Builder()
                .url(finalUrl)
                .build();

        String jsonResponse = "";

        try(Response response = new OkHttpClient().newCall(request).execute()) {
            if (response.body() == null) {
                throw new Exception();
            }
            jsonResponse = response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }

        CaptchaResult result = new Gson().fromJson(jsonResponse, CaptchaResult.class);

        return Optional.of(result);
    }
}
