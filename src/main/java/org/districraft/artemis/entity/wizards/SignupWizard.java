//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.entity.wizards;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "signup_wizard")
@EntityListeners(AuditingEntityListener.class)
public class SignupWizard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int step = 0;
    private String sessionId;
    private String username;
    private String password;
    private String email;
    private String realName;
    private String mcName;
    private String mcUUID;
    private boolean mcLegacy;
    private boolean mcDemo;
    private String mcSkinUrl;
    private String mcCapeUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public String getMcUUID() {
        return mcUUID;
    }

    public void setMcUUID(String mcUUID) {
        this.mcUUID = mcUUID;
    }

    public boolean isMcLegacy() {
        return mcLegacy;
    }

    public void setMcLegacy(boolean mcLegacy) {
        this.mcLegacy = mcLegacy;
    }

    public boolean isMcDemo() {
        return mcDemo;
    }

    public void setMcDemo(boolean mcDemo) {
        this.mcDemo = mcDemo;
    }

    public String getMcSkinUrl() {
        return mcSkinUrl;
    }

    public void setMcSkinUrl(String mcSkinUrl) {
        this.mcSkinUrl = mcSkinUrl;
    }

    public String getMcCapeUrl() {
        return mcCapeUrl;
    }

    public void setMcCapeUrl(String mcCapeUrl) {
        this.mcCapeUrl = mcCapeUrl;
    }

    public String getjSessionId() {
        return sessionId;
    }

    public void setjSessionId(String jSessionId) {
        this.sessionId = jSessionId;
    }
}
