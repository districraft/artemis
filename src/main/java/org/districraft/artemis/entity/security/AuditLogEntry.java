package org.districraft.artemis.entity.security;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class AuditLogEntry {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(nullable = false)
    private AuditActionType actionType;
    @Column(nullable = false)
    private AuditActionSecurityLevel securityLevel;
    @Column(nullable = false)
    private String actionDetail;

    @Column(nullable = false)
    private String className;
    @Column(nullable = false)
    private Timestamp tStamp = new Timestamp(System.currentTimeMillis());

    @ManyToOne
    private User sourceUser;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public AuditActionType getActionType() {
        return actionType;
    }

    public void setActionType(AuditActionType actionType) {
        this.actionType = actionType;
    }

    public AuditActionSecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(AuditActionSecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }

    public String getActionDetail() {
        return actionDetail;
    }

    public void setActionDetail(String actionDetail) {
        this.actionDetail = actionDetail;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Timestamp gettStamp() {
        return tStamp;
    }

    public void settStamp(Timestamp tStamp) {
        this.tStamp = tStamp;
    }

    public User getSourceUser() {
        return sourceUser;
    }

    public void setSourceUser(User sourceUser) {
        this.sourceUser = sourceUser;
    }
}
