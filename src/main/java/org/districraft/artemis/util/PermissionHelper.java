//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.repository.permission.PermissionAssignmentRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class PermissionHelper {
    private PermissionAssignmentRepository repository;

    public PermissionHelper(PermissionAssignmentRepository repository) {
        this.repository = repository;
    }

    /**
     * Check whether a specific user has a permission node assigned. Does not check for group permissions!
     * @param user user
     * @param node Permission node
     * @return has permission
     */
    public boolean userHasPermission(User user, String node) {
        Long count = repository.existsByUserAndNode(node, user.getId());
        return (count > 0L);
    }

    /**
     * Check whether a specific user's group has a permission node assigned. Does not work for user permissions.
     * @param user user
     * @param node Permission node
     * @return has permission
     */
    public boolean userHasPermissionByGroup(User user, String node) {
        Long count = repository.existsByGroupAndNode(node, user.getPermissionGroup().getId());
        return (count > 0L);
    }

    /**
     * Check whether a specific user has a permission node assigned. Does not check for group permissions!
     * Gets user object from SecurityContext.
     * @param node Permission node
     * @return has permission
     */
    public boolean userFromContextHasPermission(String node) {
        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = principal.getUser();

        return userHasPermission(user, node);
    }

    /**
     * Check whether a specific user's group has a permission node assigned. Does not work for user permissions.
     * Gets user object from SecurityContext.
     * @param node Permission node
     * @return has permission
     */
    public boolean userFromContextHasPermissionByGroup(String node) {
        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = principal.getUser();

        return userHasPermissionByGroup(user, node);
    }
}
