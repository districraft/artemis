//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.cms;

import com.github.slugify.Slugify;
import org.districraft.artemis.entity.statuspage.Incident;
import org.districraft.artemis.entity.statuspage.Service;
import org.districraft.artemis.entity.statuspage.ServiceGroup;
import org.districraft.artemis.repository.statuspage.IncidentRepository;
import org.districraft.artemis.repository.statuspage.IncidentUpdateRepository;
import org.districraft.artemis.repository.statuspage.ServiceGroupRepository;
import org.districraft.artemis.repository.statuspage.ServiceRepository;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.districraft.artemis.util.validation.ValidationFormatting;
import org.districraft.artemis.util.validation.ValidationResult;
import org.districraft.artemis.util.validation.ValidationRule;
import org.districraft.artemis.util.validation.ValidationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
public class CMSStatuspageAdminController {
    private ServiceGroupRepository serviceGroupRepository;
    private ServiceRepository serviceRepository;
    private IncidentRepository incidentRepository;
    private IncidentUpdateRepository incidentUpdateRepository;
    private Slugify slugify;

    public CMSStatuspageAdminController(ServiceGroupRepository serviceGroupRepository,
                                        ServiceRepository serviceRepository,
                                        IncidentRepository incidentRepository,
                                        IncidentUpdateRepository incidentUpdateRepository,
                                        Slugify slugify) {
        this.serviceGroupRepository = serviceGroupRepository;
        this.serviceRepository = serviceRepository;
        this.incidentRepository = incidentRepository;
        this.incidentUpdateRepository = incidentUpdateRepository;
        this.slugify = slugify;
    }

    @GetMapping("/statuspage-admin")
    public String getStatuspageAdmin(Model model) {
        List<ServiceGroup> serviceGroups = serviceGroupRepository.findAll();
        serviceGroups.forEach((sg) -> sg.setServices(serviceRepository.findAllByGroup(sg)));

        model.addAttribute("groups", serviceGroups);
        model.addAttribute("activePage", "/statuspage-admin");

        return "/admin/statuspage/index.html";
    }

    @GetMapping("/statuspage-admin/group/{id}")
    public String getServiceGroup(@PathVariable(name = "id") Long id, Model model) throws ResourceNotFoundException {
        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(id);

        if(!oGroup.isPresent()) {
            throw new ResourceNotFoundException();
        }

        ServiceGroup group = oGroup.get();
        group.setServices(serviceRepository.findAllByGroup(group));
        model.addAttribute("group", group);

        return "/admin/statuspage/group.html";
    }

    @GetMapping("/statuspage-admin/group/new")
    public String getNewServiceGroup(Model model) {
        model.addAttribute("activePage", "/statuspage-admin");
        return "/admin/statuspage/new-group.html";
    }

    // name, ordering, visible

    @PostMapping("/statuspage-admin/group/create")
    public String postCreateServiceGroup(@RequestParam(name = "name") String name,
                                         @RequestParam(name = "ordering") int ordering,
                                         @RequestParam(name = "visible", required = false) String visible,
                                         RedirectAttributes redirectAttributes) {

        ValidationService validationService = new ValidationService();
        validationService.add(name, "Name must not be empty", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(ordering, "Ordering must not be null", ValidationRule.NOT_NULL);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:/statuspage-admin/group/new";
        }

        ServiceGroup sg = new ServiceGroup();
        sg.setName(name);
        sg.setOrdering(ordering);
        sg.setVisible((visible != null));

        sg = serviceGroupRepository.save(sg);

        redirectAttributes.addFlashAttribute("success", "Service Group saved");
        return "redirect:/statuspage-admin/group/" + sg.getId();
    }

    @GetMapping("/statuspage-admin/group/edit/{id}")
    public String getEditServiceGroup(@PathVariable(name = "id") Long id, Model model) throws ResourceNotFoundException {
        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(id);

        if(!oGroup.isPresent()) {
            throw new ResourceNotFoundException();
        }

        model.addAttribute("group", oGroup.get());

        return "/admin/statuspage/edit-group.html";
    }

    @PostMapping("/statuspage-admin/group/update")
    public String postUpdateServiceGroup(@RequestParam(name = "name") String name,
                                         @RequestParam(name = "ordering") int ordering,
                                         @RequestParam(name = "visible", required = false) String visible,
                                         @RequestParam(name = "id") Long id,
                                         HttpServletRequest request,
                                         RedirectAttributes redirectAttributes) throws ResourceNotFoundException {

        String referer = (request.getHeader("Referer").isEmpty())? "/statuspage-admin" : request.getHeader("Referer");

        ValidationService validationService = new ValidationService();
        validationService.add(name, "Name must not be empty", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(ordering, "Ordering must not be null", ValidationRule.NOT_NULL);
        validationService.add(id, "Id must not be null", ValidationRule.NOT_NULL);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:" + referer;
        }

        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(id);

        if(!oGroup.isPresent()) {
            throw new ResourceNotFoundException();
        }

        ServiceGroup group = oGroup.get();
        group.setName(name);
        group.setVisible((visible != null));
        group.setOrdering(ordering);

        group = serviceGroupRepository.save(group);

        redirectAttributes.addFlashAttribute("success", "Service Group saved");
        return "redirect:/statuspage-admin/group/" + group.getId();
    }

    @GetMapping("/statuspage-admin/group/delete/{id}")
    public String getDeleteGroup(@PathVariable(name = "id") Long id,
                                Model model) throws ResourceNotFoundException {

        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(id);
        if(!oGroup.isPresent()) throw new ResourceNotFoundException();
        model.addAttribute("group", oGroup.get());

        return "admin/statuspage/delete-group.html";
    }

    @PostMapping("/statuspage-admin/group/destroy")
    public String postDestroyGroup(@RequestParam(name = "id") Long id,
                                   RedirectAttributes redirectAttributes) throws ResourceNotFoundException {

        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(id);
        if(!oGroup.isPresent()) throw new ResourceNotFoundException();

        serviceGroupRepository.delete(oGroup.get());

        redirectAttributes.addFlashAttribute("success", oGroup.get().getName() + " has been deleted");
        return "redirect:/statuspage-admin";
    }

    @GetMapping("/statuspage-admin/service/{id}")
    public String getService(@PathVariable(name = "id") Long id,
                             Model model) throws ResourceNotFoundException {

        Optional<Service> oService = serviceRepository.findById(id);
        if(!oService.isPresent()) throw new ResourceNotFoundException();
        Service service = oService.get();

        List<Incident> incidents = incidentRepository.findAllByServiceOrderByIdDesc(service);
        incidents.forEach((x) -> {
            x.setUpdates(incidentUpdateRepository.findAllByIncidentOrderByIdDesc(x));
            if(!x.getUpdates().isEmpty()) x.setStatus(x.getUpdates().get(0).getStatus());
        });

        model.addAttribute("service", oService.get());
        model.addAttribute("incidents", incidents);

        return "admin/statuspage/service.html";
    }

    @GetMapping("/statuspage-admin/service/new")
    public String getNewService(Model model) {
        model.addAttribute("groups", serviceGroupRepository.findAll());
        return "admin/statuspage/new-service.html";
    }

    @PostMapping("/statuspage-admin/service/create")
    public String postCreateService(@RequestParam(name = "name") String name,
                                    @RequestParam(name = "description") String description,
                                    @RequestParam(name = "ordering", defaultValue = "0") int ordering,
                                    @RequestParam(name = "group", required = false) Long group,
                                    @RequestParam(name = "visible", required = false) String visible,
                                    RedirectAttributes redirectAttributes) throws ResourceNotFoundException {

        ValidationService validationService = new ValidationService();
        validationService.add(name, "Name must be present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(description, "Description must be present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(ordering, "Order must be present", ValidationRule.NOT_NULL);
        validationService.add(group, "Group must be selected", ValidationRule.NOT_NULL);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:/statuspage-admin/service/new";
        }

        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(group);
        if(!oGroup.isPresent()) throw new ResourceNotFoundException();

        Service service = new Service();
        service.setName(name);
        service.setDescription(description);
        service.setOrdering(ordering);
        service.setGroup(oGroup.get());
        service.setEnabled((visible != null));
        service.setSlug(slugify.slugify(name));

        service = serviceRepository.save(service);

        redirectAttributes.addFlashAttribute("success", "Service created.");
        return "redirect:/statuspage-admin/service/" + service.getId();
    }

    @GetMapping("/statuspage-admin/service/edit/{id}")
    public String getEditService(@PathVariable(name = "id") Long id, Model model) throws ResourceNotFoundException {
        Optional<Service> oService = serviceRepository.findById(id);
        if(!oService.isPresent()) throw new ResourceNotFoundException();

        model.addAttribute("service", oService.get());
        model.addAttribute("groups", serviceGroupRepository.findAll());

        return "admin/statuspage/edit-service.html";
    }

    @PostMapping("/statuspage-admin/service/update")
    public String getUpdateService(@RequestParam(name = "name") String name,
                                   @RequestParam(name = "description") String description,
                                   @RequestParam(name = "ordering", defaultValue = "0") int ordering,
                                   @RequestParam(name = "group", required = false) Long group,
                                   @RequestParam(name = "visible", required = false) String visible,
                                   @RequestParam(name = "id") Long id,
                                   HttpServletRequest request,
                                   RedirectAttributes redirectAttributes) throws ResourceNotFoundException {

        String referer = (request.getHeader("Referer").isEmpty())? "/statuspage-admin" : request.getHeader("Referer");

        ValidationService validationService = new ValidationService();
        validationService.add(name, "Name must be present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(description, "Description must be present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(ordering, "Order must be present", ValidationRule.NOT_NULL);
        validationService.add(group, "Group must be selected", ValidationRule.NOT_NULL);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:" + referer;
        }

        Optional<ServiceGroup> oGroup = serviceGroupRepository.findById(group);
        if(!oGroup.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "Invalid group selected");
            return "redirect:" + referer;
        }

        if(id == null) throw new ResourceNotFoundException();

        Optional<Service> oService = serviceRepository.findById(id);
        if(!oService.isPresent()) throw new ResourceNotFoundException();
        Service service = oService.get();

        service.setName(name);
        service.setDescription(description);
        service.setOrdering(ordering);
        service.setGroup(oGroup.get());
        service.setEnabled((visible != null));

        serviceRepository.save(service);

        redirectAttributes.addFlashAttribute("success", "Service saved");
        return "redirect:/statuspage-admin";
    }
}
