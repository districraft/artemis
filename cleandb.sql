drop database if exists artemis;
create database artemis;
grant all privileges on artemis.* to artemis@localhost;
flush privileges;
