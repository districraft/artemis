//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.api;

import org.apache.groovy.util.Maps;
import org.districraft.artemis.container.*;
import org.districraft.artemis.entity.auth.ApiToken;
import org.districraft.artemis.entity.auth.MCAuthenticationRequest;
import org.districraft.artemis.enums.APIScope;
import org.districraft.artemis.repository.auth.ApiTokenRepository;
import org.districraft.artemis.repository.auth.MCAuthenticationRequestRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.util.validation.ValidationFormatting;
import org.districraft.artemis.util.validation.ValidationResult;
import org.districraft.artemis.util.validation.ValidationRule;
import org.districraft.artemis.util.validation.ValidationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Optional;

@RestController
public class VerificationAPIController {
    private ApiTokenRepository apiTokenRepository;
    private MCAuthenticationRequestRepository mcAuthenticationRequestRepository;
    private UserRepository userRepository;

    public VerificationAPIController(ApiTokenRepository apiTokenRepository,
                                     MCAuthenticationRequestRepository mcAuthenticationRequestRepository,
                                     UserRepository userRepository) {
        this.apiTokenRepository = apiTokenRepository;
        this.mcAuthenticationRequestRepository = mcAuthenticationRequestRepository;
        this.userRepository = userRepository;
    }

    @PostMapping("/api/v1/verification/get_token")
    public Object postVerificationTokenForUsername(@RequestBody TokenRequestContainer tokenRequestContainer,
                                                  HttpServletResponse response) {
        if(tokenRequestContainer == null || tokenRequestContainer.getUsername() == null || tokenRequestContainer.getUsername().isEmpty()) {
            response.setStatus(404);
            return new ApiErrorResponseContainer(new String[]{ "Username Not Found" });
        }

        Optional<MCAuthenticationRequest> oReq = mcAuthenticationRequestRepository.findByUsername(tokenRequestContainer.getUsername());
        if(!oReq.isPresent()) {
            response.setStatus(404);
            return new ApiErrorResponseContainer(new String[]{ "Username Not Found" });
        }

        MCAuthenticationRequest req = oReq.get();

        response.setStatus(200);

        return new VerificationTokenResponseContainer("success", req.getToken());
    }


    @PostMapping("/api/v1/verification/perform")
    public Object performServerSideVerification(@RequestBody ProfileVerificationRequestContainer request, HttpServletResponse response) {
        ValidationService validationService = new ValidationService();

        validationService.add(request.getApiToken(), "API Token Not Present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(request.getVerificationToken(), "Verification Token Not Present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(request.getUuid(), "UUID Not Present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(request.getCapeUrl(), "Cape URL cannot be null but can be empty.", ValidationRule.NOT_NULL);
        validationService.add(request.getSkinUrl(), "Skin URL cannot be null but may be empty.", ValidationRule.NOT_NULL);
        validationService.add(request.getUsername(), "Username Not Present", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);

        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            response.setStatus(400);
            return new ApiErrorResponseContainer(validationResult.getErrorMessages());
        }

        Optional<ApiToken> oToken = apiTokenRepository.findByToken(request.getApiToken());

        if(!oToken.isPresent()) {
            response.setStatus(401);
            return new ApiErrorResponseContainer(new String[]{ "API Token Not Present" });
        }

        ApiToken token = oToken.get();

        if(token.getScope() != APIScope.VERIFICATION) {
            response.setStatus(403);
            return new ApiErrorResponseContainer(new String[]{ "API Key Invalid For This Scope" });
        }

        Optional<MCAuthenticationRequest> oAuthReq = mcAuthenticationRequestRepository.findByToken(request.getVerificationToken());

        if(!oAuthReq.isPresent()) {
            response.setStatus(400);
            return new ApiErrorResponseContainer(new String[]{ "Invalid Verification Token" });
        }

        MCAuthenticationRequest req = oAuthReq.get();

        if(!request.getUsername().equalsIgnoreCase(req.getUsername())) {
            response.setStatus(400);
            return new ApiErrorResponseContainer(new String[]{ "Invalid token and username combination." });
        }

        req.setUuid(request.getUuid());
        req.setSkinUrl(request.getSkinUrl());
        req.setCapeUrl(request.getCapeUrl());

        mcAuthenticationRequestRepository.save(req);

        response.setStatus(200);

        return new ApiEmptySuccessReponseContainer("success");
    }
}
