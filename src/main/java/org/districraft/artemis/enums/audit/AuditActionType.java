package org.districraft.artemis.enums.audit;

public enum AuditActionType {
    SYSTEM("teal"),
    ADMIN("blue"),
    CREATE("green"),
    UPDATE("yellow"),
    DELETE("red"),
    READ("pink");

    private String color;

    AuditActionType(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
