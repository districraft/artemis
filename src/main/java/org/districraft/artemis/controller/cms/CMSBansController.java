//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.cms;

import org.districraft.artemis.entity.mcserver.Ban;
import org.districraft.artemis.repository.mcserver.BanRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class CMSBansController {

    private BanRepository banRepository;

    public CMSBansController(BanRepository banRepository) {
        this.banRepository = banRepository;
    }

    @GetMapping("/cms/bans")
    public String getBansPage(Model model) {
        List<Ban> bans = banRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
        model.addAttribute("bans", bans);

        return "cms/bans.html";
    }
}
