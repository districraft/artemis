//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.service;

import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Random;

@Service
public class RandomStringGenerator {
    private static final String ALLOWED_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "0123456789"
            + "abcdefghijklmnopqrstuvxyz";

    private final Random random = new SecureRandom();

    public RandomStringGenerator() {}

    public String nextString() {
        return nextString(32);
    }

    public String nextString(int length) throws IllegalArgumentException {
        if(length < 1) throw new IllegalArgumentException("Length must be > 1");
        char buffer[] = new char[length];

        for(int idx = 0; idx < buffer.length; ++idx) {
            buffer[idx] = ALLOWED_CHARS.charAt(random.nextInt(ALLOWED_CHARS.length()));
        }

        return new String(buffer);
    }
}
