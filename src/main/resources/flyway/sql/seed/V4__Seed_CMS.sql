#     Artemis
#     Copyright (C) 2020, Jakub Sycha
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

DELETE FROM post_category WHERE TRUE;
DELETE FROM post WHERE TRUE;
DELETE FROM simple_page WHERE TRUE;

INSERT INTO post_category (id, name, slug) VALUES (1, 'Test Category', 'test-category');

INSERT INTO post (id, content, created_at, slug, title, updated_at, author_id, category_id) VALUES
(
    1,
    '<p>Hello World</p>',
    CURRENT_TIMESTAMP,
    'test-post',
    'Test Post',
    NULL,
    1,
    1
);

INSERT INTO simple_page (id, content, created_at, slug, title, updated_at) VALUES
(
    1,
    'Hello World',
    CURRENT_TIMESTAMP,
    'test-page',
    'Test Page',
    NULL
);