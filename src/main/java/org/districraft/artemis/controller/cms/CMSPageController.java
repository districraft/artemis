//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.cms;

import org.districraft.artemis.entity.cms.Post;
import org.districraft.artemis.entity.cms.SimplePage;
import org.districraft.artemis.repository.cms.PostRepository;
import org.districraft.artemis.repository.cms.SimplePageRepository;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Controller
public class CMSPageController {

    private SimplePageRepository simplePageRepository;
    private PostRepository postRepository;

    public CMSPageController(SimplePageRepository simplePageRepository, PostRepository postRepository) {
        this.simplePageRepository = simplePageRepository;
        this.postRepository = postRepository;
    }

    @GetMapping("/cms")
    public String getIndex(Model model) {
        List<Post> posts = postRepository.findAllByOrderByIdDesc();
        model.addAttribute("posts", posts);

        return "cms/homepage.html";
    }

    @GetMapping("/cms/page/{slug}")
    public String getPageBySlug(@PathVariable("slug") String slug, Model model) throws ResourceNotFoundException {
        Optional<SimplePage> oPage = simplePageRepository.findBySlug(slug);

        if(!oPage.isPresent()) throw new ResourceNotFoundException(true);

        SimplePage page = oPage.get();
        model.addAttribute("page", page);

        return "cms/simple_page.html";
    }
}
