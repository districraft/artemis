//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.districraft.artemis.repository.issue.IssueReportRepository;
import org.districraft.artemis.repository.NotificationRepository;
import org.districraft.artemis.tags.ArtemisDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Configuration
public class ThymeleafLayoutConfig {
    private ArtemisDialect ad;

    public ThymeleafLayoutConfig(ArtemisDialect ad) {
        this.ad = ad;
    }

    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addDialect(new LayoutDialect());
        templateEngine.addDialect(new SpringSecurityDialect());
        templateEngine.addDialect(ad);
        return templateEngine;
    }

//    @Bean
//    public ArtemisDialect artemisDialect(NotificationRepository notificationRepository, IssueReportRepository issueReportRepository) {
//        return new ArtemisDialect(notificationRepository, issueReportRepository);
//    }
}
