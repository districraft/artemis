//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.tags;

import org.districraft.artemis.repository.issue.IssueReportRepository;
import org.districraft.artemis.repository.NotificationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

import java.util.HashSet;
import java.util.Set;

@Service
public class ArtemisDialect extends AbstractProcessorDialect {
    private final NotificationRepository notificationRepository;
    private final IssueReportRepository issueReportRepository;

    public ArtemisDialect(final NotificationRepository notificationRepository, final IssueReportRepository issueReportRepository) {
        super(
                "Artemis Dialect",
                "artemis",
                1000
        );

        this.notificationRepository = notificationRepository;
        this.issueReportRepository = issueReportRepository;
    }

    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new NotificationsTagProcessor(dialectPrefix, notificationRepository));
        processors.add(new NotificationCountTagProcessor(dialectPrefix, notificationRepository));
        processors.add(new IssueOperationsButtonsTagProcessor(dialectPrefix, issueReportRepository));
        return processors;
    }

//    @Bean
//    public ArtemisDialect artemisDialect(NotificationRepository notificationRepository, IssueReportRepository issueReportRepository) {
//        return new ArtemisDialect(notificationRepository, issueReportRepository);
//    }
}
