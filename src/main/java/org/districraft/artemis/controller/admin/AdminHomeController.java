package org.districraft.artemis.controller.admin;

import org.districraft.artemis.enums.Authority;
import org.districraft.artemis.util.PermUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminHomeController {
    @GetMapping("/admin")
    public String getIndex(Model model) {
        model.addAttribute("activePage", "admin/dash");

        return "admin/home/index";
    }
}
