package org.districraft.artemis.controller.admin.cms;

import com.github.slugify.Slugify;
import org.districraft.artemis.entity.cms.PostCategory;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.districraft.artemis.repository.cms.PostCategoryRepository;
import org.districraft.artemis.service.AuditService;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.MessageFormat;
import java.util.Optional;

@Controller
public class CMSCategoriesAdminController {
    private PostCategoryRepository postCategoryRepository;
    private Slugify slugify;
    private AuditService auditService;

    public CMSCategoriesAdminController(PostCategoryRepository postCategoryRepository, Slugify slugify, AuditService auditService) {
        this.postCategoryRepository = postCategoryRepository;
        this.slugify = slugify;
        this.auditService = auditService;
    }

    @GetMapping("/admin/cms/categories")
    public String getIndex(Model model) {
        model.addAttribute("categories", postCategoryRepository.findAll());
        model.addAttribute("activePage", "admin/cms/categories");
        return "admin/cms/categories/index";
    }

    @GetMapping("/admin/cms/categories/new")
    public String getNew(Model model) {
        model.addAttribute("activePage", "admin/cms/categories");
        return "admin/cms/categories/new";
    }

    @GetMapping("/admin/cms/categories/edit/{id}")
    public String getEdit(@PathVariable("id") Long id, Model model) throws ResourceNotFoundException {
        Optional<PostCategory> oCategory = postCategoryRepository.findById(id);
        if(!oCategory.isPresent()) throw new ResourceNotFoundException();
        model.addAttribute("category", oCategory.get());
        model.addAttribute("activePage", "admin/cms/categories");
        return "admin/cms/categories/edit";
    }

    @GetMapping("/admin/cms/categories/delete/{id}")
    public String getDelete(@PathVariable("id") Long id, Model model) throws ResourceNotFoundException {
        Optional<PostCategory> oCategory = postCategoryRepository.findById(id);
        if(!oCategory.isPresent()) throw new ResourceNotFoundException();
        model.addAttribute("category", oCategory.get());
        model.addAttribute("activePage", "admin/cms/categories");
        return "admin/cms/categories/delete";
    }

    @PostMapping("/admin/cms/categories/create")
    public String postCreate(@RequestParam("name") String name, RedirectAttributes redirectAttributes) {
        if(name.equalsIgnoreCase("")) {
            redirectAttributes.addFlashAttribute("error", "The name must not be empty");
            return "redirect:/admin/cms/categories/new";
        }

        String slug = slugify.slugify(name);
        Optional<PostCategory> oCategory = postCategoryRepository.findBySlug(slug);

        if(oCategory.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "A category with that name already exists.");
            return "redirect:/admin/cms/categories/new";
        }

        PostCategory category = new PostCategory();
        category.setName(name);
        category.setSlug(slug);

        category = postCategoryRepository.save(category);

        auditService.log(AuditActionType.CREATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Category created with id {0}", category.getId().toString()),
                CMSCategoriesAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The category has been saved");

        return "redirect:/admin/cms/categories";
    }

    @PostMapping("/admin/cms/categories/update")
    public String postUpdate(@RequestParam("id") Long id, @RequestParam("name") String name, RedirectAttributes redirectAttributes) throws ResourceNotFoundException {
        if(name.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Name must not be empty");
            return "redirect:/admin/cms/categories/edit/" + id;
        }

        Optional<PostCategory> oCategory = postCategoryRepository.findById(id);
        if(!oCategory.isPresent()) throw new ResourceNotFoundException();

        PostCategory category = oCategory.get();
        category.setName(name);
        category.setSlug(slugify.slugify(name));
        postCategoryRepository.save(category);

        auditService.log(AuditActionType.UPDATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Category updated with id {0}", category.getId().toString()),
                CMSCategoriesAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The category has been updated");
        return "redirect:/admin/cms/categories";
    }

    @PostMapping("/admin/cms/categories/drop")
    public String postDrop(@RequestParam("id") Long id, RedirectAttributes redirectAttributes) {
        Optional<PostCategory> oCategory = postCategoryRepository.findById(id);
        if(!oCategory.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "You're trying to delete a category that doesn't exist");
            return "redirect:/admin/cms/categories";
        }

        postCategoryRepository.delete(oCategory.get());

        auditService.log(AuditActionType.DELETE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Category deleted with id {0}", oCategory.get().getId().toString()),
                CMSCategoriesAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The category has been deleted.");
        return "redirect:/admin/cms/categories";
    }
}
