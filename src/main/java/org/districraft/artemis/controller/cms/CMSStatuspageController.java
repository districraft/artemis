//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.cms;

import org.districraft.artemis.entity.statuspage.Service;
import org.districraft.artemis.entity.statuspage.ServiceGroup;
import org.districraft.artemis.repository.statuspage.ServiceGroupRepository;
import org.districraft.artemis.repository.statuspage.ServiceRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class CMSStatuspageController {
    private ServiceRepository serviceRepository;
    private ServiceGroupRepository serviceGroupRepository;

    public CMSStatuspageController(ServiceRepository serviceRepository, ServiceGroupRepository serviceGroupRepository) {
        this.serviceRepository = serviceRepository;
        this.serviceGroupRepository = serviceGroupRepository;
    }

    @GetMapping("/cms/status")
    public String getStatuspage(Model model) {
        List<ServiceGroup> serviceGroups = serviceGroupRepository.findAll();
        serviceGroups.forEach((sg) -> sg.setServices(serviceRepository.findAllByGroup(sg)));
        model.addAttribute("groups", serviceGroups);
        return "cms/statuspage.html";
    }
}
