# ARTEMIS
Artemis is an all in one web presence solution for Minecraft servers.
It combines a simple and easy to use CMS with many different server-connected
self service features for your users, such as a support desk, ban management system,
whtelist signup and more.

At the moment, Artemis has three parts. Artemis - the app itself,
Bifrost - An interface between Artemis and Mojang's Yggdrasil for
Minecraft account authentication and artemis-connector, a plugin for
Sponge that connects Artemis to your server.

This application is distributed under the GPLv3 License. See LICENSE.txt for more.

At the moment, Artemis runs on Java 8. [Amazon Corretto](https://aws.amazon.com/corretto/) is recommended,
however any OpenJDK distribution should work. Contributors are kindly asked to test their changes with Corretto 8
before creating a merge request.

## Deploying with Docker
**Prerequisities:**
 - Docker
 - Docker Compose
 - Gnu Make
 - This guide assumes a Linux environment

**Running the Docker Development Environment:**
 1. ```$ make prepare-docker```
 2. Edit the docker-application.properties file in /docker/artemis
 3. ```$ make docker```
 4. Add ```127.0.0.1 devsite.local artemis.devsite.local``` to your hosts file
 5. Open http://devsite.local and http://artemis.devsite.local in your browser
 6. Log in with ```admin``` and ```P@ssw0rd!```
 7. (Optional but strongly recommended) Import the sample data. A prompt should appear
 when you first open the app. If not, go to `http://artemis.devsite.local/debug/seed/`

There is a non-zero chance MySQL will fail to initialize. If this hapens,
kill the containers with CTRL+C, run `make clean-docker` and then `make docker` to redeploy.

Please keep in mind persistent storage is not implemented at the moment
and all changes will be lost when redeployed.