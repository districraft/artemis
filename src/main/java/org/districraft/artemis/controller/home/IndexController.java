//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.home;

import org.districraft.artemis.enums.Authority;
import org.districraft.artemis.util.PermUtil;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/")
public class IndexController {
    private Flyway flyway;

    public IndexController(Flyway flyway) {
        this.flyway = flyway;
    }

    @GetMapping("/")
    public String getIndex(Model model, @RequestParam(value = "import_success", required = false) Boolean success) {

        if(flyway.info().current() == null) {
            return "redirect:/import-data";
        }

        model.addAttribute("import_success", success);
        model.addAttribute("activePage", "/");

        model.addAttribute("auth", PermUtil.getCurrentUserAuthority());

        return "home/index.html";
    }

    @GetMapping("/import-data")
    public String getImportData(Model model) {
        if(flyway.info().current() != null) return "redirect:/";
        return "home/import-data.html";
    }
}
