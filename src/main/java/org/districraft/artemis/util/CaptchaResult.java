//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util;

public class CaptchaResult {
    private boolean success;
    private String[] fail_codes;

    public CaptchaResult(boolean success, String[] fail_codes) {
        this.success = success;
        this.fail_codes = fail_codes;
    }

    public CaptchaResult() {}

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String[] getFail_codes() {
        return fail_codes;
    }

    public void setFail_codes(String[] fail_codes) {
        this.fail_codes = fail_codes;
    }
}
