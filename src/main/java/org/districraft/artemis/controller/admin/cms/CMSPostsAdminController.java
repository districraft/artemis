package org.districraft.artemis.controller.admin.cms;

import com.github.slugify.Slugify;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.entity.cms.Post;
import org.districraft.artemis.entity.cms.PostCategory;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.repository.cms.PostCategoryRepository;
import org.districraft.artemis.repository.cms.PostRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.districraft.artemis.service.AuditService;
import org.districraft.artemis.util.InsufficientPermissionException;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.districraft.artemis.util.validation.ValidationFormatting;
import org.districraft.artemis.util.validation.ValidationResult;
import org.districraft.artemis.util.validation.ValidationRule;
import org.districraft.artemis.util.validation.ValidationService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class CMSPostsAdminController {
    private PostRepository postRepository;
    private PostCategoryRepository postCategoryRepository;
    private UserRepository userRepository;
    private AuditService auditService;

    public CMSPostsAdminController(PostRepository postRepository,
                                   PostCategoryRepository postCategoryRepository,
                                   UserRepository userRepository,
                                   AuditService auditService) {
        this.postCategoryRepository = postCategoryRepository;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.auditService = auditService;
    }

    @GetMapping("/admin/cms/posts")
    public String getIndex(Model model) {
        List<Post> posts = postRepository.findAll();
        List<PostCategory> categories = postCategoryRepository.findAll();

        model.addAttribute("posts", posts);
        model.addAttribute("categories", categories);
        model.addAttribute("activePage", "admin/cms/posts");

        return "admin/cms/posts/index";
    }

    @GetMapping("/admin/cms/posts/new")
    public String getNew(Model model) {
        List<PostCategory> categories = postCategoryRepository.findAll();
        model.addAttribute("categories", categories);
        model.addAttribute("activePage", "admin/cms/posts");

        return "admin/cms/posts/new";
    }

    @PostMapping("/admin/cms/posts/create")
    public String postCreatePost(@RequestParam(name = "title") String title,
                                 @RequestParam(name = "content") String content,
                                 @RequestParam(name = "category") Long category,
                                 Model model,
                                 RedirectAttributes redirectAttributes,
                                 HttpServletRequest request) throws InsufficientPermissionException {

        String referer = request.getHeader("Referer");
        boolean redir = false;

        ValidationService validationService = new ValidationService();

        validationService.add(title, "Title must be present.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(content, "Content must be present.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(category, "Category must be present.", ValidationRule.NOT_NULL);

        ValidationResult validationResult = validationService.validate();

        if (validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            if (referer != null && !referer.isEmpty()) {
                return "redirect:" + referer;
            } else {
                return "redirect:/admin/cms/posts";
            }
        }

        Slugify slg = new Slugify();
        String slug = slg.slugify(title);

        Optional<Post> oPost = postRepository.findBySlug(slug);
        Optional<PostCategory> postCategory = postCategoryRepository.findById(category);

        if (!postCategory.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "The selected category does not exist.");
            redir = true;
        }

        if (oPost.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "A post with that title already exists.");
            redir = true;
        }

        if (redir) {
            if (referer != null && !referer.isEmpty()) {
                return "redirect:" + referer;
            } else {
                return "redirect:/admin/cms/posts";
            }
        }

        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = principal.getUser();
        Optional<User> oUser = userRepository.findById(user.getId());

        if (!oUser.isPresent()) throw new InsufficientPermissionException(222);

        user = oUser.get();

        Post post = new Post();

        post.setTitle(title);
        post.setSlug(slug);
        post.setContent(content);
        post.setCategory(postCategory.get());
        post.setCreatedAt(new Timestamp(System.currentTimeMillis()));
        post.setAuthor(user);

        post = postRepository.save(post);

        auditService.log(AuditActionType.CREATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Post created with id {0}", post.getId().toString()),
                CMSPostsAdminController.class,
                null,
                user);

        redirectAttributes.addFlashAttribute("success", "The post was successfully saved. <a href=\"" + post.getSlug() + "\">Open</a>");
        return "redirect:/admin/cms/posts";
    }

    @GetMapping("/admin/cms/posts/edit/{slug}")
    public String getEditPost(@PathVariable(name = "slug") String slug, Model model) throws ResourceNotFoundException {
        Optional<Post> oPost = postRepository.findBySlug(slug);
        if (!oPost.isPresent()) throw new ResourceNotFoundException();
        Post post = oPost.get();

        List<PostCategory> categories = postCategoryRepository.findAll();

        model.addAttribute("post", post);
        model.addAttribute("categories", categories);
        model.addAttribute("activePage", "admin/cms/posts");

        return "admin/cms/posts/edit";
    }

    @PostMapping("/admin/cms/posts/update")
    public String postUpdatePost(@RequestParam(name = "title") String title,
                                 @RequestParam(name = "slug") String slug,
                                 @RequestParam(name = "content") String content,
                                 @RequestParam(name = "category") Long category,
                                 Model model,
                                 RedirectAttributes redirectAttributes,
                                 HttpServletRequest request
    ) throws ResourceNotFoundException {
        Optional<Post> oPost = postRepository.findBySlug(slug);
        Optional<PostCategory> oCat = Optional.empty();

        if (!oPost.isPresent()) throw new ResourceNotFoundException();

        List<String> errors = new ArrayList<>();

        if (title == null || title.isEmpty()) errors.add("Title must not be empty");
        if (slug == null || slug.isEmpty()) errors.add("Slug must not be empty");
        if (category == null) {
            errors.add("You must select a category");
        } else {
            oCat = postCategoryRepository.findById(category);
            if (!oCat.isPresent()) errors.add("Invalid category selected");
        }

        if (!errors.isEmpty()) {
            StringBuilder err = new StringBuilder();
            errors.forEach((x) -> {
                err.append("<ul>\n");
                err.append("<li>").append(x).append("</li>\n");
                err.append("</ul>\n");
            });
            redirectAttributes.addFlashAttribute("error", err.toString());
            return "redirect:" + request.getHeader("Referer");
        }

        Post post = oPost.get();
        PostCategory newCategory = oCat.get();

        post.setTitle(title);
        post.setContent(content);
        post.setCategory(newCategory);
        post.setUpdatedAt(new Timestamp(System.currentTimeMillis()));

        post = postRepository.save(post);

        auditService.log(AuditActionType.UPDATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Post updated with id {0}", post.getId().toString()),
                CMSPostsAdminController.class,
                null,
                post.getAuthor());

        return "redirect:/admin/cms/posts";
    }

    @GetMapping("/admin/cms/posts/delete/{id}")
    public String getDeleteItem(@PathVariable("id") Long id,
                                HttpServletRequest request,
                                Model model) throws ResourceNotFoundException {

        Post post = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

        model.addAttribute("id", id);
        model.addAttribute("post", post);
        model.addAttribute("activePage", "admin/cms/posts");

        return "admin/cms/posts/delete";
    }

    @PostMapping("/admin/cms/posts/drop")
    public String postDeletePost(@RequestParam("id") Long id, RedirectAttributes redirectAttributes) throws ResourceNotFoundException {
        Optional<Post> oPost = postRepository.findById(id);

        if (!oPost.isPresent()) throw new ResourceNotFoundException();

        Post post = oPost.get();
        postRepository.delete(post);

        auditService.log(AuditActionType.DELETE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Post deleted with id {0}", post.getId().toString()),
                CMSPostsAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The post has been deleted");

        return "redirect:/admin/cms/posts";
    }
}
