//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.auth;

import org.districraft.artemis.entity.auth.MCAuthenticationRequest;
import org.districraft.artemis.entity.auth.MinecraftProfile;
import org.districraft.artemis.entity.auth.Profile;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.repository.auth.MCAuthenticationRequestRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.districraft.artemis.service.RandomStringGenerator;
import org.districraft.artemis.util.InsufficientPermissionException;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.districraft.artemis.util.validation.ValidationFormatting;
import org.districraft.artemis.util.validation.ValidationResult;
import org.districraft.artemis.util.validation.ValidationRule;
import org.districraft.artemis.util.validation.ValidationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Controller
public class ProfileController {

    private UserRepository userRepository;
    private MCAuthenticationRequestRepository mcAuthenticationRequestRepository;
    private RandomStringGenerator randomStringGenerator;

    @Value("${artemis.minecraft.verifyServerIP}")
    private String verificationServerIP;

    public ProfileController(UserRepository userRepository,
                             MCAuthenticationRequestRepository mcAuthenticationRequestRepository,
                             RandomStringGenerator randomStringGenerator) {
        this.userRepository = userRepository;
        this.mcAuthenticationRequestRepository = mcAuthenticationRequestRepository;
        this.randomStringGenerator = randomStringGenerator;
    }

    @GetMapping("/profile")
    public String getProfilePage(Model model) throws InsufficientPermissionException {
        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> oUser = userRepository.findById(principal.getUser().getId());

        if(!oUser.isPresent()) throw new InsufficientPermissionException(9974);
        User user = oUser.get();

        Profile profile = user.getProfile();
        MinecraftProfile minecraftProfile = profile.getMinecraftProfile();

        model.addAttribute("user", user);
        model.addAttribute("profile", profile);
        model.addAttribute("mcprofile", minecraftProfile);

        return "auth/profile.html";
    }

    @GetMapping("/profile/server-verification")
    public String getServerVerification(@RequestParam(name = "token", required = false, defaultValue = "") String token, Model model) {
        model.addAttribute("ip", verificationServerIP);
        model.addAttribute("token", token);
        return "auth/verification.html";
    }

    @PostMapping("/profile/verify_minecraft")
    public String postVerifyMinecraft(@RequestParam(name = "username") String username, RedirectAttributes redirectAttributes) throws InsufficientPermissionException, ResourceNotFoundException {
        ValidationService validationService = new ValidationService();
        validationService.add(username, "Username must not be null", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:/profile";
        }

        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> oUser = userRepository.findById(principal.getUser().getId());

        if(!oUser.isPresent()) throw new InsufficientPermissionException(9975);
        User user = oUser.get();

        if(user.getProfile().getMinecraftProfile().isConnected()) throw new ResourceNotFoundException();

        String token = randomStringGenerator.nextString(16);

        // If the token already exists, try a new one. Realistically this will probably never happen...
        while(mcAuthenticationRequestRepository.findByToken(token).isPresent()) {
            token = randomStringGenerator.nextString(16);
        }

        MCAuthenticationRequest request = new MCAuthenticationRequest();
        request.setToken(randomStringGenerator.nextString(16));
        request.setUsername(username);
        request.setUser(user);

        mcAuthenticationRequestRepository.save(request);

        return "redirect:/profile/server-verification";
    }

    @PostMapping("/profile/finish_verification")
    public String postFinishVerification(@RequestParam(name = "token") String token, RedirectAttributes redirectAttributes) throws InsufficientPermissionException {
        ValidationService validationService = new ValidationService();
        validationService.add(token, "Token must not be empty.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        ValidationResult validationResult = validationService.validate();

        if(validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));
            return "redirect:/profile/server-verification";
        }

        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> oUser = userRepository.findById(principal.getUser().getId());

        if(!oUser.isPresent()) throw new InsufficientPermissionException(9975);
        User user = oUser.get();

        if(user.getProfile().getMinecraftProfile().isConnected()) {
            redirectAttributes.addFlashAttribute("error", "Your profile is already connected and verified.");
            return "redirect:/profile";
        }

        Optional<MCAuthenticationRequest> oRequest = mcAuthenticationRequestRepository.findByToken(token);

        if(!oRequest.isPresent()) {
            redirectAttributes.addFlashAttribute("Invalid token, please try again.");
            return "redirect:/profile/server-verification";
        }

        MCAuthenticationRequest request = oRequest.get();

        if(request.getUsername() == null || request.getUsername().isEmpty() || request.getUuid() == null || request.getUuid().isEmpty()) {
            mcAuthenticationRequestRepository.delete(request);
            redirectAttributes.addFlashAttribute("error", "Unknown error has occurred. Please try the verification process again.");
        }

        MinecraftProfile mcProf = user.getProfile().getMinecraftProfile();

        mcProf.setConnected(true);
        mcProf.setName(request.getUsername());
        mcProf.setUuid(request.getUuid());
        mcProf.setSkinUrl(request.getSkinUrl());
        mcProf.setCapeUrl(request.getCapeUrl());

        userRepository.save(user);
        mcAuthenticationRequestRepository.delete(request);

        redirectAttributes.addFlashAttribute("success", "Nice! Your Artemis account is now connected to your Minecraft account and all Artemis features are available.");

        return "redirect:/profile";
    }
}
