//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.auth;

import org.districraft.artemis.entity.auth.ActivationLink;
import org.districraft.artemis.entity.auth.MinecraftProfile;
import org.districraft.artemis.entity.auth.Profile;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.entity.permission.PermissionAssignment;
import org.districraft.artemis.entity.permission.PermissionGroup;
import org.districraft.artemis.repository.auth.ActivationLinkRepository;
import org.districraft.artemis.repository.auth.UserRepository;
import org.districraft.artemis.repository.permission.PermissionAssignmentRepository;
import org.districraft.artemis.repository.permission.PermissionGroupRepository;
import org.districraft.artemis.service.GenericEmailSender;
import org.districraft.artemis.service.RandomStringGenerator;
import org.districraft.artemis.util.CaptchaResult;
import org.districraft.artemis.util.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class SignupController {

    private static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$";
    private static final String PASSWORD_REGEX = "^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[.!@#$%^&*\\(\\)\\[\\]\\{\\}\\\\\\/\\,]).{6,20})$";

    private UserRepository userRepository;
    private ActivationLinkRepository activationLinkRepository;
    private RandomStringGenerator randomStringGenerator;
    private GenericEmailSender genericEmailSender;
    private PasswordEncoder passwordEncoder;
    private CaptchaUtil captchaUtil;
    private PermissionAssignmentRepository permissionAssignmentRepository;
    private PermissionGroupRepository permissionGroupRepository;

    @Value("${artemis.captcha.siteKey}")
    private String captchaSitekey;

    @Value("${artemis.app.baseUrl}")
    private String baseUrl;

    public SignupController(UserRepository userRepository,
                            ActivationLinkRepository activationLinkRepository,
                            RandomStringGenerator randomStringGenerator,
                            GenericEmailSender genericEmailSender,
                            PasswordEncoder passwordEncoder,
                            CaptchaUtil captchaUtil,
                            PermissionAssignmentRepository permissionAssignmentRepository,
                            PermissionGroupRepository permissionGroupRepository) {
        this.userRepository = userRepository;
        this.activationLinkRepository = activationLinkRepository;
        this.randomStringGenerator = randomStringGenerator;
        this.genericEmailSender = genericEmailSender;
        this.passwordEncoder = passwordEncoder;
        this.captchaUtil = captchaUtil;
        this.permissionAssignmentRepository = permissionAssignmentRepository;
        this.permissionGroupRepository = permissionGroupRepository;
    }

    @GetMapping("/signup")
    public String getIndex(Model model) {
        model.addAttribute("hcaptchaSitekey", captchaSitekey);
        return "signup/index.html";
    }

    @PostMapping("/create-account")
    public String postCreateUser(
            @RequestParam("fullname") String fullName,
            @RequestParam("email") String email,
            @RequestParam("password") String password,
            @RequestParam("password_check") String passwordCheck,
            @RequestParam(value = "terms", required = false) String terms,
            @RequestParam("username") String username,
            @RequestParam("mtcaptcha-verifiedtoken") String captchaToken,
            Model model,
            RedirectAttributes redirectAttributes) {

        List<String> errors = new ArrayList<>();

        if (email.isEmpty() || !email.matches(EMAIL_REGEX)) errors.add("Invalid Email");
        if (password.isEmpty() || !password.matches(PASSWORD_REGEX)) errors.add("Password too weak");
        if (!password.equals(passwordCheck)) errors.add("Passwords don't match");
        if (terms != null && !terms.equalsIgnoreCase("agree")) errors.add("You need to accept the terms");
        if (username.isEmpty()) errors.add("Invalid username");

        Optional<User> existingUser = userRepository.findByUsername(username);
        if (existingUser.isPresent()) errors.add("That username is taken");

        existingUser = userRepository.findByEmail(email);
        if (existingUser.isPresent()) errors.add("An account with that e-mail address already exists");

        Optional<CaptchaResult> captchaResult = captchaUtil.verify(captchaToken);

        if(!captchaResult.isPresent()) { // API Didn't work 5586
            errors.add("Invalid captcha response (5586)");
        } else {
            if(!captchaResult.get().isSuccess()) errors.add("Invalid captcha response (9955)");
        }

        if (!errors.isEmpty()) {
            StringBuilder err = new StringBuilder();
            errors.forEach((x) -> {
                err.append("<ul>\n");
                err.append("<li>").append(x).append("</li>\n");
                err.append("</ul>\n");
            });
            redirectAttributes.addFlashAttribute("error", err.toString());
            return "redirect:/signup";
        }

        User user = new User();
        Profile profile = new Profile();
        MinecraftProfile minecraftProfile = new MinecraftProfile();

        profile.setAvatar("");
        profile.setRealName(fullName);

        minecraftProfile.setConnected(false);
        minecraftProfile.setDemo(true);
        minecraftProfile.setLegacy(true);

        profile.setMinecraftProfile(minecraftProfile);

        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        user.setRole("USER");
        user.setActivated(false);
        user.setProfile(profile);

        user = userRepository.save(user);

        Optional<PermissionGroup> permissionGroup = permissionGroupRepository.findByName("USER");
        if(permissionGroup.isPresent()) {
            user.setPermissionGroup(permissionGroup.get());
        }

        ActivationLink al = new ActivationLink();
        al.setUser(user);

        String token = randomStringGenerator.nextString();

        // If the token already exists, try a new one. Realistically this will probably never happen...
        while(activationLinkRepository.findByActivationCode(token).isPresent()) {
            token = randomStringGenerator.nextString();
        }

        al.setActivationCode(token);
        al = activationLinkRepository.save(al);

        StringBuilder messageBody = new StringBuilder();
        messageBody.append("<h2>Your Artemis account has been created</h2>\n");
        messageBody.append("<p>Click the link below to activate your account.</p>\n");
        messageBody.append("<p><small>The link is only valid for 24 hours!</small></p>\n");
        messageBody.append("<hr>\n");
        messageBody.append(MessageFormat.format("<p><a href=\"{0}/activate-account?activationKey={1}\">Activate your account</a></p>\n", baseUrl, al.getActivationCode()));

        try {
            genericEmailSender.sendGenericEmail("Artemis account created",
                    messageBody.toString(), true, email);
        } catch (MessagingException ignored) {}

        return "redirect:/signup-success";
    }

    @GetMapping("/signup-success")
    public String getSignupSuccess() {
        return "signup/success.html";
    }

    @GetMapping("/activate-account")
    public String getActivateAccount(@RequestParam("activationKey") String activationKey, RedirectAttributes redirectAttributes) {
        if(activationKey == null || activationKey.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Invalid activation key.");
            return "redirect:/activation-done";
        }

        Optional<ActivationLink> oAl = activationLinkRepository.findByActivationCode(activationKey);

        if(!oAl.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "Invalid activation key.");
            return "redirect:/activation-done";
        }

        ActivationLink al = oAl.get();

        User user = al.getUser();
        user.setActivated(true);

        userRepository.save(user);
        activationLinkRepository.delete(al);

        redirectAttributes.addFlashAttribute("success", "Your account has been activated. You may now log in.");

        return "redirect:/activation-done";
    }

    @GetMapping("/activation-done")
    public String getActivationDone() {
        return "signup/activation-done.html";
    }

}
