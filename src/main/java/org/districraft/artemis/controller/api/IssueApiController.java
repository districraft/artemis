//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.api;

import org.districraft.artemis.entity.issue.IssueComment;
import org.districraft.artemis.entity.issue.IssueReport;
import org.districraft.artemis.entity.permission.PermissionGroup;
import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.enums.IssueResolutionStatus;
import org.districraft.artemis.repository.issue.IssueCommentRepository;
import org.districraft.artemis.repository.issue.IssueReportRepository;
import org.districraft.artemis.repository.permission.PermissionGroupRepository;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.districraft.artemis.util.InsufficientPermissionException;
import org.districraft.artemis.util.PermissionHelper;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class IssueApiController {
    private IssueReportRepository issueReportRepository;
    private IssueCommentRepository issueCommentRepository;

        private PermissionHelper permissionHelper;

        private Map<String, Long> groups = new HashMap<>();

    public IssueApiController(IssueReportRepository issueReportRepository,
                IssueCommentRepository issueCommentRepository,
                PermissionGroupRepository permissionGroupRepository,
                PermissionHelper permissionHelper) {

            this.issueCommentRepository = issueCommentRepository;
            this.issueReportRepository = issueReportRepository;
            this.permissionHelper = permissionHelper;

            Optional<PermissionGroup> oAdmin = permissionGroupRepository.findByName("ADMIN");
            Optional<PermissionGroup> oGM = permissionGroupRepository.findByName("GM");
            Optional<PermissionGroup> oMod = permissionGroupRepository.findByName("MOD");
            Optional<PermissionGroup> oUser = permissionGroupRepository.findByName("USER");

            oAdmin.ifPresent(permissionGroup -> groups.put("ADMIN", permissionGroup.getId()));
            oGM.ifPresent(permissionGroup -> groups.put("GM", permissionGroup.getId()));
            oMod.ifPresent(permissionGroup -> groups.put("MOD", permissionGroup.getId()));
            oUser.ifPresent(permissionGroup -> groups.put("USER", permissionGroup.getId()));
        }

        @RequestMapping(value = "/api/v1/issues", method = {RequestMethod.GET, RequestMethod.POST})
        public String getIndex() throws ResourceNotFoundException {
            throw new ResourceNotFoundException();
        }

        // TODO: DCXARTEMIS-44 -- Disabled until further notice
//        @GetMapping("/api/v1/issues/start-work/{id}")
//        public Object getStartWork(@PathVariable(name = "id") Long id, HttpServletRequest request) throws ResourceNotFoundException, InsufficientPermissionException {
//            return setStatusAndRedirect(request, id, IssueResolutionStatus.IN_PROGRESS);
//        }
//
//        @GetMapping("/api/v1/issues/resolve/{id}")
//        public Object getResolve(@PathVariable(name = "id") Long id, HttpServletRequest request) throws ResourceNotFoundException, InsufficientPermissionException {
//            return setStatusAndRedirect(request, id, IssueResolutionStatus.CLOSED);
//        }
//
//        @GetMapping("/api/v1/issues/reopen/{id}")
//        public Object getReopen(@PathVariable(name = "id") Long id, HttpServletRequest request) throws ResourceNotFoundException, InsufficientPermissionException {
//            return setStatusAndRedirect(request, id, IssueResolutionStatus.OPEN);
//        }
//
//        @GetMapping("/api/v1/issues/wont-fix/{id}")
//        public Object getWontfix(@PathVariable(name = "id") Long id, HttpServletRequest request) throws ResourceNotFoundException, InsufficientPermissionException {
//            return setStatusAndRedirect(request, id, IssueResolutionStatus.WONT_FIX);
//        }
//
//        @GetMapping("/api/v1/issues/reject/{id}")
//        public Object getReject(@PathVariable(name = "id") Long id, HttpServletRequest request) throws ResourceNotFoundException, InsufficientPermissionException {
//            return setStatusAndRedirect(request, id, IssueResolutionStatus.REJECTED);
//        }
//
//        @PostMapping("/api/v1/issues/comment")
//        public Object postComment(
//                @RequestParam(name = "comment", required = true) String content,
//                @RequestParam(name = "id", required = true) Long id,
//                HttpServletRequest request) throws ResourceNotFoundException {
//
//            ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            User user = principal.getUser();
//
//            String referer = request.getHeader("Referer");
//            Optional<IssueReport> oReport = issueReportRepository.findById(id);
//            if(!oReport.isPresent()) throw new ResourceNotFoundException();
//
//            IssueReport report = oReport.get();
//
//            IssueComment comment = new IssueComment();
//            comment.setAuthor(user);
//            comment.setReport(report);
//            comment.setCreatedAt(new Timestamp(System.currentTimeMillis()));
//            comment.setContent(content);
//
//            issueCommentRepository.save(comment);
//
//            return "redirect:" + referer;
//        }
//
//        private String setStatusAndRedirect(HttpServletRequest request, Long id, IssueResolutionStatus status) throws ResourceNotFoundException, InsufficientPermissionException {
//            ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            User user = principal.getUser();
//
//            Optional<IssueReport> oReport = issueReportRepository.findById(id);
//            if(!oReport.isPresent()) throw new ResourceNotFoundException();
//            IssueReport report = oReport.get();
//
//            if(user.getPermissionGroup().getName().equals(report.getEscalationLevel().getName())) {
//                if(!permissionHelper.userHasPermissionByGroup(user, "issues.changestatus.own"))
//                    throw new InsufficientPermissionException(99776);
//            } else {
//                if(!permissionHelper.userHasPermissionByGroup(user, "issues.changestatus.any"))
//                    throw new InsufficientPermissionException(99777);
//            }
//
//            String referer = request.getHeader("Referer");
//            report.setResolutionStatus(status);
//            issueReportRepository.save(report);
//            return "redirect:" + referer;
//        }
    }
