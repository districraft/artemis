//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util.validation;

public class ValidationResult {
    private boolean errors;
    private String[] errorMessages;

    public ValidationResult() {}

    public ValidationResult(boolean errors, String[] errorMessages) {
        this.errors = errors;
        this.errorMessages = errorMessages;
    }

    public String[] getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(String[] errorMessages) {
        this.errorMessages = errorMessages;
    }

    public boolean hasErrors() {
        return errors;
    }

    public void setErrors(boolean errors) {
        this.errors = errors;
    }

    public String getFormattedErrors(ValidationFormatting validationFormatting) {
        String result;

        switch (validationFormatting) {
            case COMMA_PLAINTEXT:
                result = ValidationResultFormatter.formatAsPlaintext(errorMessages, ", ");
                break;
            case NEWLINE_PLAINTEXT:
                result = ValidationResultFormatter.formatAsPlaintext(errorMessages, "\n");
                break;
            case HTML_LIST:
                result = ValidationResultFormatter.formatAsHTMLList(errorMessages);
                break;
            case CSV:
                result = ValidationResultFormatter.formatAsPlaintext(errorMessages, ",");
                break;
            case JSON:
                result = ValidationResultFormatter.formatAsJSON(errorMessages);
                break;
            default:
                result = ValidationResultFormatter.formatAsPlaintext(errorMessages, " ");
                break;
        }

        return result;
    }
}
