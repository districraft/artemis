package org.districraft.artemis.controller.admin.cms;

import com.github.slugify.Slugify;
import org.districraft.artemis.entity.cms.SimplePage;
import org.districraft.artemis.enums.audit.AuditActionSecurityLevel;
import org.districraft.artemis.enums.audit.AuditActionType;
import org.districraft.artemis.repository.cms.SimplePageRepository;
import org.districraft.artemis.service.AuditService;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.districraft.artemis.util.validation.ValidationFormatting;
import org.districraft.artemis.util.validation.ValidationResult;
import org.districraft.artemis.util.validation.ValidationRule;
import org.districraft.artemis.util.validation.ValidationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@Controller
public class CMSPagesAdminController {
    private SimplePageRepository simplePageRepository;
    private AuditService auditService;

    public CMSPagesAdminController(SimplePageRepository simplePageRepository, AuditService auditService) {
        this.simplePageRepository = simplePageRepository;
        this.auditService = auditService;
    }

    @GetMapping("/admin/cms/pages")
    public String getIndex(Model model) {
        List<SimplePage> pages = simplePageRepository.findAll();
        model.addAttribute("pages", pages);
        model.addAttribute("activePage", "admin/cms/pages");

        return "admin/cms/pages/index";
    }

    @GetMapping("/admin/cms/pages/new")
    public String getNewPage(Model model) {
        model.addAttribute("activePage", "admin/cms/pages");
        return "admin/cms/pages/new";
    }

    @GetMapping("/admin/cms/pages/edit/{slug}")
    public String getEditPage(@PathVariable(name = "slug") String slug,
                              Model model) throws ResourceNotFoundException {

        Optional<SimplePage> oPage = simplePageRepository.findBySlug(slug);
        if (!oPage.isPresent()) throw new ResourceNotFoundException();
        SimplePage page = oPage.get();
        model.addAttribute("page", page);
        model.addAttribute("activePage", "admin/cms/pages");
        return "admin/cms/pages/edit";
    }

    @GetMapping("/admin/cms/pages/delete/{id}")
    public String getDeletePage(@PathVariable(name = "id") Long id,
                                Model model,
                                RedirectAttributes redirectAttributes) {

        Optional<SimplePage> oPage = simplePageRepository.findById(id);

        if(!oPage.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "You're trying to delete a page that doesn't exist");
            return "redirect:/admin/cms/pages";
        }

        model.addAttribute("page", oPage.get());
        model.addAttribute("activePage", "admin/cms/pages");
        return "admin/cms/pages/delete";
    }

    @PostMapping("/admin/cms/pages/create")
    public String postCreatePage(@RequestParam(name = "title") String title,
                                 @RequestParam(name = "content") String content,
                                 HttpServletRequest request,
                                 RedirectAttributes redirectAttributes) {

        String referer = request.getHeader("Referer");

        ValidationService validationService = new ValidationService();

        validationService.add(title, "Title must be present.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(content, "Content must be present.", ValidationRule.NOT_NULL);

        ValidationResult validationResult = validationService.validate();

        if (validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));

            if (!referer.isEmpty()) {
                return "redirect:" + referer;
            } else {
                return "redirect:/admin/cms/pages";
            }
        }

        Slugify slg = new Slugify();
        String slug = slg.slugify(title);

        Optional<SimplePage> oPage = simplePageRepository.findBySlug(slug);

        if (oPage.isPresent()) {
            redirectAttributes.addFlashAttribute("error", "A page with that title already exists.");

            if (!referer.isEmpty()) {
                return "redirect:" + referer;
            } else {
                return "redirect:/admin/cms/pages";
            }
        }

        SimplePage page = new SimplePage();
        page.setTitle(title);
        page.setSlug(slug);
        page.setContent(content);
        page.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        page = simplePageRepository.save(page);

        StringBuilder sb = new StringBuilder();
        sb.append("The page has been created. ");
        sb.append("<a href=\"/cms/page/");
        sb.append(slug);
        sb.append("\">Open</a>");

        auditService.log(AuditActionType.CREATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Page created with id {0}", page.getId().toString()),
                CMSPagesAdminController.class);

        redirectAttributes.addFlashAttribute("success", sb.toString());
        return "redirect:/admin/cms/pages";
    }

    @PostMapping("/admin/cms/pages/update")
    public String postUpdatePage(@RequestParam(name = "title") String title,
                                 @RequestParam(name = "content") String content,
                                 @RequestParam(name = "id") Long id,
                                 RedirectAttributes redirectAttributes,
                                 HttpServletRequest request) throws ResourceNotFoundException {
        String referer = request.getHeader("Referer");
        ValidationService validationService = new ValidationService();

        validationService.add(title, "Title must not be empty.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(content, "Content must not be empty.", ValidationRule.NOT_NULL, ValidationRule.NOT_EMPTY);
        validationService.add(id, "ID must not be null.", ValidationRule.NOT_NULL);

        ValidationResult validationResult = validationService.validate();

        if (validationResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", validationResult.getFormattedErrors(ValidationFormatting.HTML_LIST));

            if (!referer.isEmpty()) {
                return "redirect:" + referer;
            } else {
                return "redirect:/admin/cms/pages";
            }
        }

        Optional<SimplePage> oPage = simplePageRepository.findById(id);

        if (!oPage.isPresent()) throw new ResourceNotFoundException();

        SimplePage page = oPage.get();

        page.setTitle(title);
        page.setContent(content);

        page = simplePageRepository.save(page);

        auditService.log(AuditActionType.UPDATE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Page updated with id {0}", page.getId().toString()),
                CMSPagesAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The page has been updated.");

        return "redirect:/admin/cms/pages";
    }

    @PostMapping("/admin/cms/pages/drop")
    public String postDropPage(@RequestParam("id") Long id, RedirectAttributes redirectAttributes) throws ResourceNotFoundException {
        Optional<SimplePage> oPage = simplePageRepository.findById(id);
        if (!oPage.isPresent()) throw new ResourceNotFoundException();
        SimplePage page = oPage.get();
        simplePageRepository.delete(page);

        auditService.log(AuditActionType.DELETE,
                AuditActionSecurityLevel.ADMIN,
                MessageFormat.format("A CMS Page deleted with id {0}", page.getId().toString()),
                CMSPagesAdminController.class);

        redirectAttributes.addFlashAttribute("success", "The page has been deleted");
        return "redirect:/admin/cms/pages";
    }
}
