//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.repository.mcserver;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.entity.mcserver.Ban;
import org.districraft.artemis.enums.mcserver.BanType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BanRepository extends JpaRepository<Ban, Long> {
    List<Ban> findAllByType(BanType type);

    Optional<Ban> findByTargetUsername(String username);
    Optional<Ban> findByTargetUUID(String uuid);
}
