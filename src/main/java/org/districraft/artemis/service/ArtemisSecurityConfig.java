//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ArtemisSecurityConfig extends WebSecurityConfigurerAdapter {
    private ArtemisUserDetailsService artemisUserDetailsService;

    public ArtemisSecurityConfig(ArtemisUserDetailsService artemisUserDetailsService) {
        super();
        this.artemisUserDetailsService = artemisUserDetailsService;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().and().userDetailsService(artemisUserDetailsService);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()

                .antMatchers("/login").permitAll()
                .antMatchers("/perform_login").permitAll()
                .antMatchers("/password-reset", "/perform_password_reset", "/password-recovery", "/perform_password_recovery").permitAll()
                .antMatchers("/signup", "/signup/**", "/create-account", "/signup-success", "/activate-account", "/activation-done").anonymous()

                .antMatchers("/css/**", "/js/**", "/img/**", "/adminlte/**", "/tabler/**").permitAll()

                .antMatchers("/cms", "/cms/*", "/cms/post", "/cms/post/*", "/cms/page/**", "/cms/status", "/cms/status/**").permitAll()

                .antMatchers("/api/v1/verification", "/api/v1/verification/**", "/api/v1/banStatus").permitAll()

                .antMatchers("/admin/audit", "/admin/system-config", "/admin/users/groups", "/admin/users/permissions")
                    .hasAnyAuthority("ADMIN")

                .antMatchers("/admin", "/admin/**").hasAnyAuthority("ADMIN", "GM")

                .antMatchers("/").fullyAuthenticated()
                .antMatchers("/**").fullyAuthenticated()

                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/perform_login")
                .defaultSuccessUrl("/", true)
                .failureUrl("/login?error=true")
                .and()
                .logout()
                .logoutUrl("/perform_logout")
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/login")
                .and()
                .rememberMe().key("lODdNvdRyevMzYdPbRPjxXduUxe7GzPjhlHfEXDV");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
