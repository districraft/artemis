//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller;

import org.districraft.artemis.util.InsufficientPermissionException;
import org.districraft.artemis.util.ResourceNotFoundException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(ResourceNotFoundException.class)
    public String handleResourceNotFoundException(HttpServletResponse response, ResourceNotFoundException exception) {
        response.setStatus(404);
        if(exception.isCms()) {
            return "cms/error/404.html";
        } else {
            return "error/404.html";
        }
    }

    @ExceptionHandler(InsufficientPermissionException.class)
    public String handleInsufficientPermissionException(HttpServletResponse response, InsufficientPermissionException exception, Model model) {
        model.addAttribute("errorCode", exception.getErrorCode());
        response.setStatus(403);
        return "error/403.html";
    }
}
