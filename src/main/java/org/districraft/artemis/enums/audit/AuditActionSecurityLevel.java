package org.districraft.artemis.enums.audit;

public enum AuditActionSecurityLevel {
    SYSTEM("teal"),
    ADMIN("blue"),
    USER("purple"),
    ANON("grey");

    private String color;

    AuditActionSecurityLevel(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
