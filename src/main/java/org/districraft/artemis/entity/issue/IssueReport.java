//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.entity.issue;

import org.districraft.artemis.entity.auth.User;
import org.districraft.artemis.enums.IssueCategory;
import org.districraft.artemis.enums.IssueEscalationLevel;
import org.districraft.artemis.enums.IssueResolutionStatus;
import org.districraft.artemis.enums.IssueSeverity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.Instant;

@Entity
public class IssueReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User author;

    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Column(nullable = false)
    private IssueCategory category;
    @Column(nullable = false)
    private IssueSeverity severity = IssueSeverity.LOW;
    @Column(nullable = false)
    private IssueEscalationLevel escalationLevel = IssueEscalationLevel.MOD;
    @Column(nullable = false)
    private IssueResolutionStatus resolutionStatus = IssueResolutionStatus.OPEN;

    private String tags;
    private String assignee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public IssueCategory getCategory() {
        return category;
    }

    public void setCategory(IssueCategory category) {
        this.category = category;
    }

    public IssueSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(IssueSeverity severity) {
        this.severity = severity;
    }

    public IssueEscalationLevel getEscalationLevel() {
        return escalationLevel;
    }

    public void setEscalationLevel(IssueEscalationLevel escalationLevel) {
        this.escalationLevel = escalationLevel;
    }

    public IssueResolutionStatus getResolutionStatus() {
        return resolutionStatus;
    }

    public void setResolutionStatus(IssueResolutionStatus resolutionStatus) {
        this.resolutionStatus = resolutionStatus;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getStatusBadge() {
        return getStatusBadge(false);
    }

    public String getStatusBadge(boolean small) {
        String badgeColor;
        String badgeText;

        switch(resolutionStatus) {
            case OPEN:
                badgeColor = "blue";
                badgeText = "Open";
                break;
            case IN_PROGRESS:
                badgeColor = "azure";
                badgeText = "In Progress";
                break;
            case CLOSED:
                badgeColor = "green";
                badgeText = "Resolved";
                break;
            case WONT_FIX:
                badgeColor = "orange";
                badgeText = "Won't Fix";
                break;
            case REJECTED:
                badgeColor = "red";
                badgeText = "Rejected";
                break;
            default:
                badgeColor = "grey";
                badgeText = "Unknown";
                break;
        }

        String smallTag = (small)? "tag-small" : "";

        return MessageFormat.format("<span class=\"tag {0} tag-{1}\">{2}</span>", smallTag, badgeColor, badgeText);
    }

    public String getEscalationBadge() {
        return getEscalationBadge(false);
    }
    public String getEscalationBadge(boolean small) {
        String badgeColor;
        String badgeText;

        switch(escalationLevel) {
            case MOD:
                badgeColor = "purple";
                badgeText = "Mod";
                break;
            case GM:
                badgeColor = "red";
                badgeText = "GM";
                break;
            case ADMIN:
                badgeColor = "azure";
                badgeText = "Admin";
                break;
            default:
                badgeColor = "grey";
                badgeText = "Unknown";
                break;
        }

        String smallTag = (small)? "tag-small" : "";

        return MessageFormat.format("<span class=\"tag {0} tag-{1}\">{2}</span>", smallTag, badgeColor, badgeText);
    }

    public String getSeverityBadge() {
        return getSeverityBadge(false);
    }
    public String getSeverityBadge(boolean small) {
        String badgeColor;
        String badgeText;

        switch (severity) {
            case LOWEST:
                badgeColor = "green";
                badgeText = "Lowest";
                break;
            case LOW:
                badgeColor = "lime";
                badgeText = "Low";
                break;
            case MEDIUM:
                badgeColor = "yellow";
                badgeText = "Medium";
                break;
            case HIGH:
                badgeColor = "orange";
                badgeText = "High";
                break;
            case SEVERE:
                badgeColor = "red";
                badgeText = "Severe";
                break;
            default:
                badgeColor = "grey";
                badgeText = "Unknown";
                break;
        }
        String smallTag = (small)? "tag-small" : "";

        return MessageFormat.format("<span class=\"tag {0} tag-{1}\">{2}</span>", smallTag, badgeColor, badgeText);
    }

    public String getTypeBadge() {
        return getTypeBadge(false);
    }
    public String getTypeBadge(boolean small) {
        String badgeColor;
        String badgeText;

        switch (category) {
            case BUG:
                badgeColor = "red";
                badgeText = "Bug";
                break;
            case GAME:
                badgeColor = "lime";
                badgeText = "Game";
                break;
            case IDEA:
                badgeColor = "azure";
                badgeText = "Game";
                break;
            case MISC:
                badgeColor = "purple";
                badgeText = "Other";
                break;
            default:
                badgeColor = "grey";
                badgeText = "Unknown";
                break;
        }

        String smallTag = (small)? "tag-small" : "";

        return MessageFormat.format("<span class=\"tag {0} tag-{1}\">{2}</span>", smallTag, badgeColor, badgeText);
    }
}
