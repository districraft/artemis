//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller;

import org.districraft.artemis.entity.Notification;
import org.districraft.artemis.repository.NotificationRepository;
import org.districraft.artemis.util.NotificationType;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
public class DebugController {
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private Flyway flyway;

    @GetMapping("/debug/create-notification")
    public Object createNotification() {
        Notification notification = new Notification();
        notification.setGlobal(true);
        notification.setType(NotificationType.INFO);
        notification.setCreated_at(new Date());
        notification.setMessage("Testing notification");

        notificationRepository.save(notification);

        return new HashMap<String, String>(){{ put("status", "OK"); }};
    }

    @GetMapping("/debug/list-notifications")
    public List<String> getNotifications() {
        List<Notification> notifications = notificationRepository.findAllByGlobal(true);

        List<String> result = new ArrayList<>();

        notifications.forEach((x) -> { result.add(x.getMessage()); });

        return result;
    }

    @GetMapping("/debug/seed")
    public ModelAndView getSeedTestData(RedirectAttributes redirectAttributes) {
        flyway.baseline();
        flyway.migrate();
        redirectAttributes.addAttribute("import_success", true);
        ModelAndView view = new ModelAndView();
        view.setViewName("redirect:/");
        return view;
    }
}
