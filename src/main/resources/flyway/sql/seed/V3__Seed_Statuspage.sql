#     Artemis
#     Copyright (C) 2020, Jakub Sycha
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- Core Services
insert into service_group (id, name, ordering, slug, visible) VALUES (1, 'Core Services', 0, 'core-services', 1);
insert into service (id, description, enabled, name, ordering, slug,  group_id) VALUES
(
    1,
    'The Districraft website',
    1,
    'Website',
    0,
    'website',
    1
);

insert into service (id, description, enabled, name, ordering, slug, group_id) VALUES
(
    2,
    'The Artemis website',
    1,
    'Artemis',
    1,
    'artemis',
    1
);
-- End Core Services

-- MC Network
insert into service_group (id, name, ordering, slug, visible) VALUES (2, 'Minecraft Server Network', 1, 'network', 1);
insert into service (id, description, enabled, name, ordering, slug, group_id) VALUES
(
    4,
    'The Lobby server',
    1,
    'Lobby Server',
    0,
    'lobby',
    2
);
insert into service (id, description, enabled, name, ordering, slug, group_id) VALUES
(
    5,
    'The Authentication server',
    1,
    'Authentication Server',
    1,
    'auth',
    2
);
insert into service (id, description, enabled, name, ordering, slug, group_id) VALUES
(
    6,
    'Main server node 1',
    1,
    'Node 1',
    2,
    'node-1',
    2
);
insert into service (id, description, enabled, name, ordering, slug, group_id) VALUES
(
    7,
    'Main server node 2',
    1,
    'Node 2',
    3,
    'node-2',
    2
);
-- End MC Network