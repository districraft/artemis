//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.util.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationService {
    private List<ValidationPair> pairs = new ArrayList<>();

    public void add(Object validationItem, String invalidMessage, ValidationRule... validationRules) {
        pairs.add(new ValidationPair(validationItem, invalidMessage, validationRules));
    }

    public void addDirectly(ValidationPair validationPair) {
        pairs.add(validationPair);
    }

    public void addMultiple(List<ValidationPair> validationPairs) {
        pairs.addAll(validationPairs);
    }

    public ValidationResult validate() {
        ValidationResult result = new ValidationResult();
        result.setErrors(false);
        List<String> errors = new ArrayList<>();

        for(ValidationPair pair : pairs) {
            if(pair.getValidationRule().length < 1) continue;

            boolean error = false;

            for(ValidationRule rule : pair.getValidationRule()) {
                switch(rule) {
                    case NOT_EMPTY:
                        try {
                            if(((String) pair.getValidationSubject()).isEmpty()) {
                                error = true;
                                errors.add(pair.getInvalidMessage());
                                break;
                            }
                        } catch (Exception ex) {
                            error = true;
                            errors.add("The supplied object does not support the isEmpty() method and therefore cannot be used with the NOT_EMPTY rule.");
                            break;
                        }
                        break;
                    case NOT_NULL:
                        if(pair.getValidationSubject() == null) {
                            error = true;
                            errors.add(pair.getInvalidMessage());
                            break;
                        }
                    default: continue;
                }

                if(error) break;
            }
        }

        if(!errors.isEmpty()) {
            result.setErrors(true);
            String[] resArray = new String[errors.size()];
            result.setErrorMessages(errors.toArray(resArray));
        }

        return result;
    }
}
