//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.controller.api;

import org.districraft.artemis.container.ApiEmptySuccessReponseContainer;
import org.districraft.artemis.container.ApiErrorResponseContainer;
import org.districraft.artemis.container.BanStatusRequestContainer;
import org.districraft.artemis.entity.auth.ApiToken;
import org.districraft.artemis.entity.mcserver.Ban;
import org.districraft.artemis.enums.APIScope;
import org.districraft.artemis.repository.auth.ApiTokenRepository;
import org.districraft.artemis.repository.mcserver.BanRepository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
public class BansApiController {

    private ApiTokenRepository apiTokenRepository;
    private BanRepository banRepository;

    public BansApiController(ApiTokenRepository apiTokenRepository, BanRepository banRepository) {
        this.apiTokenRepository = apiTokenRepository;
        this.banRepository = banRepository;
    }

    @PostMapping("/api/v1/banStatus")
    public Object postBanStatus(@RequestBody BanStatusRequestContainer body, HttpServletResponse response) {
        if(body == null || body.getApiKey() == null || body.getUuid() == null) {
            response.setStatus(400);
            return new ApiErrorResponseContainer(new String[]{ "Invalid request." });
        }

        Optional<ApiToken> oToken = apiTokenRepository.findByToken(body.getApiKey());

        if(!oToken.isPresent()) {
            response.setStatus(401);
            return new ApiErrorResponseContainer(new String[] { "Invalid API key." });
        }

        ApiToken apiToken = oToken.get();

        if(apiToken.getScope() != APIScope.BAN) {
            response.setStatus(401);
            return new ApiErrorResponseContainer(new String[] { "Bad API key." });
        }

        Optional<Ban> oBan = banRepository.findByTargetUUID(body.getUuid());

        if(!oBan.isPresent()) {
            response.setStatus(404);
            return new ApiEmptySuccessReponseContainer("User is not banned.");
        }

        response.setStatus(200);
        return oBan.get();
    }
}
