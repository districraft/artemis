//    Artemis
//    Copyright (C) 2020, Jakub Sycha
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package org.districraft.artemis.tags;

import org.districraft.artemis.entity.Notification;
import org.districraft.artemis.repository.NotificationRepository;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.List;

public class NotificationCountTagProcessor extends AbstractAttributeTagProcessor {
    private static final String ATTR_NAME = "notifcount";
    private static final int PRECEDENCE = 10000;

    private NotificationRepository notificationRepository;

    public NotificationCountTagProcessor(final String dialectPrefix, NotificationRepository notificationRepository) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                null,
                false,
                ATTR_NAME,
                true,
                PRECEDENCE,
                true
        );

        this.notificationRepository = notificationRepository;
    }

    @Override
    protected void doProcess(ITemplateContext iTemplateContext, IProcessableElementTag iProcessableElementTag, AttributeName attributeName, String s, IElementTagStructureHandler iElementTagStructureHandler) {
        List<Notification> notificationList = notificationRepository.findAllByGlobal(true);

        iElementTagStructureHandler.setBody(String.valueOf(notificationList.size()), false);
    }
}
