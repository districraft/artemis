package org.districraft.artemis.util;

import org.districraft.artemis.enums.Authority;
import org.districraft.artemis.service.ArtemisUserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.List;

public class PermUtil {
    public static boolean verifyCurrentUserAuthority(Authority authority) {
        ArtemisUserPrincipal principal = (ArtemisUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == null) return false;
        for(GrantedAuthority a : principal.getAuthorities()) {
            if(a.getAuthority().equalsIgnoreCase(authority.getName())) {
                return true;
            }
        }
        return false;
    }

    public static String getCurrentUserAuthorityAsString() {
        return getCurrentUserAuthority().getName();
    }

    public static Authority getCurrentUserAuthority() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<SimpleGrantedAuthority> auths = (List<SimpleGrantedAuthority>) authentication.getAuthorities();
        if(auths.isEmpty()) return Authority.ANON;
        return Authority.valueOf(auths.get(0).getAuthority());
    }
}
